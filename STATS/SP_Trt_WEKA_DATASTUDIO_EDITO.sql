CREATE TABLE #TMP_CAMPAIGNS_EDITO_WEKA (
	CAMPAIGNID INT
);
CREATE TABLE #TMP_LINKS_EDITO_WEKA (
	CAMPAIGNID INT,
	CITEMID INT, 
	NB INT
);
INSERT INTO  #TMP_CAMPAIGNS_EDITO_WEKA (CAMPAIGNID)
select TOP 100  ID from campaigns T1 with (nolock) 
where 
name LIKE 'NL_Action-sociale%'
or 
name LIKE 'NL_Education-%'
or 
name LIKE 'NL_Marchés-publics-%'
or 
name LIKE 'NL_Sante-%'
or 
name LIKE 'NL_Ressources-humaines-%'
or 
name LIKE 'NL-Hebdo-Weka%';

INSERT INTO #TMP_LINKS_EDITO_WEKA (CAMPAIGNID,CITEMID,NB)
select CAMPAIGNID,CITEMID, count(*) AS NB
from flags T1 with (nolock)
where exists (select 1 from #TMP_CAMPAIGNS_EDITO_WEKA T2 WHERE T1.CAMPAIGNID=T2.campaignid)
and CLISTID is not null
GROUP BY CAMPAIGNID,CITEMID

select T1.*, T2.ID_SOURCE,T2.TYPE_CONTENU,TITRE,LIEN,T3.NAME,T3.RUN_DT FROM #TMP_LINKS_EDITO_WEKA T1
INNER JOIN ARTICLES_WEKA_NL_CONTENUS T2 with (nolock) ON T1.CITEMID=T2.ID
inner join campaigns T3 with (nolock) ON T1.CAMPAIGNID=T3.ID
