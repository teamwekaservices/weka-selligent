@DELTADAYS INT
AS
BEGIN
SET NOCOUNT ON;

MERGE DATA_WEKA_STATS_DATASTUDIO AS T
USING (select cast(sent_dt as date) as DT,ACTIONQUEUE.campaignid,count(*) as NB_CIBLE,sum(case when smtperr=250 then 1 else 0 end) as NB_DELIVRE,sum(case when smtperr=250 then 0 else 1 end) as NB_BOUNCES  
from ACTIONQUEUE with (nolock)
where exists (select 1 from campaigns with (nolock) where campaigns.id=ACTIONQUEUE.campaignid and campaigns.category in ('WEKA_ACTU','WEKA_COBRAND','WEKA_FID','WEKA_LB','WEKA_PRODUITS','WEKA_WEBCONF','WKJOBS_CANDIDATS','WKJOBS_RECRUTEURS'))
and sent_dt  >= cast(getdate()-@DELTADAYS as date) AND sent_dt < cast(getdate() as date)
group by cast(sent_dt as date),ACTIONQUEUE.campaignid) AS S
ON (T.DT=S.DT AND T.CAMPAIGNID=S.CAMPAIGNID AND T.NOM_STAT='WEKA_MKG')
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (NOM_STAT
		,[DT]
		,CAMPAIGNID
		,[NB_CIBLE]
		,[NB_DELIVRE]
		,[NB_BOUNCES])
		 VALUES ('WEKA_MKG'
			   ,S.[DT]
			   ,S.[CAMPAIGNID]
			   ,S.[NB_CIBLE]
			   ,S.[NB_DELIVRE]
			   ,S.NB_BOUNCES) 
	WHEN MATCHED
		THEN 
			UPDATE SET T.[NB_CIBLE] = S.[NB_CIBLE]
			   ,T.[NB_DELIVRE]=S.NB_DELIVRE
			   ,T.[NB_BOUNCES]=S.NB_BOUNCES;
			   


MERGE DATA_WEKA_STATS_DATASTUDIO AS T
USING (select cast(dt as date) as DT,flags.campaignid, count(*) as NB_CLICS, count(distinct(flags.userid)) as NB_U_CLICS
from flags with (nolock)
where exists ( select 1 from campaigns with (nolock) where campaigns.id=flags.campaignid and campaigns.category in ('WEKA_ACTU','WEKA_COBRAND','WEKA_FID','WEKA_LB','WEKA_PRODUITS','WEKA_WEBCONF','WKJOBS_CANDIDATS','WKJOBS_RECRUTEURS'))
and dt  >= cast(getdate()-@DELTADAYS as date) AND dt < cast(getdate() as date)
and probeid>200
group by cast(dt as date),flags.campaignid) AS S
ON (T.DT=S.DT AND T.CAMPAIGNID=S.CAMPAIGNID AND T.NOM_STAT='WEKA_MKG')
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (NOM_STAT
		,[DT]
		,[CAMPAIGNID]
		,[NB_CLICS]
		,[NB_U_CLICS])
		 VALUES ('WEKA_MKG'
			   ,S.[DT]
			   ,S.[CAMPAIGNID]
			   ,S.[NB_CLICS]
			   ,S.NB_U_CLICS)
	WHEN MATCHED
		THEN 
			UPDATE SET T.[NB_CLICS] = S.[NB_CLICS]
			   ,T.[NB_U_CLICS]=S.NB_U_CLICS;			   
			   
			   

MERGE DATA_WEKA_STATS_DATASTUDIO AS T
USING (select cast(dt as date) as DT,flags.campaignid, count(*) as NB_OUVERTURES, count(distinct(flags.userid)) as NB_U_OUVERTURES
from flags with (nolock)
where exists ( select 1 from campaigns with (nolock) where campaigns.id=flags.campaignid and campaigns.category in ('WEKA_ACTU','WEKA_COBRAND','WEKA_FID','WEKA_LB','WEKA_PRODUITS','WEKA_WEBCONF','WKJOBS_CANDIDATS','WKJOBS_RECRUTEURS'))
and dt  >= cast(getdate()-@DELTADAYS as date) AND dt < cast(getdate() as date)
and probeid=-1
group by cast(dt as date),flags.campaignid,TRIGGERID) AS S
ON (T.DT=S.DT AND T.CAMPAIGNID=S.CAMPAIGNID AND T.NOM_STAT='WEKA_MKG')
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (NOM_STAT
		,[DT]
		,[CAMPAIGNID]
		,[NB_U_OUVERTURES]
		,[NB_OUVERTURES])
		 VALUES ('WEKA_MKG'
			   ,S.[DT]
			   ,S.[CAMPAIGNID]
			   ,S.[NB_U_OUVERTURES]
			   ,S.NB_OUVERTURES) 
	WHEN MATCHED
		THEN 
			UPDATE SET T.[NB_U_OUVERTURES] = S.[NB_U_OUVERTURES]
			   ,T.[NB_OUVERTURES]=S.NB_OUVERTURES;
			   

MERGE INTO DATA_WEKA_STATS_DATASTUDIO AS T
USING (
SELECT CAMPAIGNID,max(VIEWCOUNT) AS VIEWCOUNT,max(UVIEWCOUNT) AS UVIEWCOUNT,max(TARGETCOUNT) AS TARGETCOUNT,max(DELIVERYCOUNT) AS DELIVERYCOUNT,max(BOUNCECOUNT) AS BOUNCECOUNT,
CAMPAIGNS.NAME,max(CLICKCOUNT) AS CLICKCOUNT,max(UCLICKCOUNT) AS UCLICKCOUNT FROM sim_reporting_flowmetrics WITH (NOLOCK)
INNER JOIN CAMPAIGNS WITH (NOLOCK) ON sim_reporting_flowmetrics.CAMPAIGNID=CAMPAIGNS.ID AND campaigns.category in ('WEKA_ACTU','WEKA_COBRAND','WEKA_FID','WEKA_LB','WEKA_PRODUITS','WEKA_WEBCONF','WKJOBS_CANDIDATS','WKJOBS_RECRUTEURS')
group by campaignid, name) AS S
ON (T.CAMPAIGNID=S.CAMPAIGNID AND T.NOM_STAT='WEKA_MKG_CUMUL')
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (DT,CAMPAIGNID,NB_OUVERTURES,NOM_STAT,NB_U_OUVERTURES,NB_CIBLE,NB_DELIVRE,NB_BOUNCES,NOM_CAMPAGNE,NB_CLICS,NB_U_CLICS)
		 VALUES (getdate(),S.CAMPAIGNID,S.VIEWCOUNT,'WEKA_MKG_CUMUL',S.UVIEWCOUNT,S.TARGETCOUNT,S.DELIVERYCOUNT,S.BOUNCECOUNT,
S.NAME,S.CLICKCOUNT,S.UCLICKCOUNT)
	WHEN MATCHED
		THEN 
			UPDATE SET 
				T.DT=getdate(),
				T.NB_OUVERTURES=S.VIEWCOUNT,
				T.NB_U_OUVERTURES=S.UVIEWCOUNT,
				T.NB_CIBLE=S.TARGETCOUNT,
				T.NB_DELIVRE=S.DELIVERYCOUNT,
				T.NB_BOUNCES=S.BOUNCECOUNT,
				T.NOM_CAMPAGNE=S.NAME,
				T.NB_CLICS=S.CLICKCOUNT,
				T.NB_U_CLICS=S.UCLICKCOUNT;			   
			   
UPDATE DATA_WEKA_STATS_DATASTUDIO 
SET NOM_CAMPAGNE=(select name from campaigns with (nolock) where campaigns.id=DATA_WEKA_STATS_DATASTUDIO.campaignid)
WHERE NOM_CAMPAGNE IS NULL;

UPDATE DATA_WEKA_STATS_DATASTUDIO 
SET START_DT=(select RUN_DT from campaigns with (nolock) where campaigns.id=DATA_WEKA_STATS_DATASTUDIO.campaignid)
WHERE START_DT IS NULL AND NOM_STAT='WEKA_MKG_COMM';

	UPDATE DATA_WEKA_STATS_DATASTUDIO 
	SET CAMPAGNE_CATEGORY=(select category from campaigns with (nolock) where campaigns.id=DATA_WEKA_STATS_DATASTUDIO.campaignid)
	WHERE CAMPAGNE_CATEGORY IS NULL;
	

END
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


--------------
-- Stats cumulées des journeys avec le détail par email


MERGE INTO DATA_WEKA_STATS_DATASTUDIO AS T
USING (
SELECT CAMPAIGNID,max(VIEWCOUNT) AS VIEWCOUNT,max(UVIEWCOUNT) AS UVIEWCOUNT,max(TARGETCOUNT) AS TARGETCOUNT,max(DELIVERYCOUNT) AS DELIVERYCOUNT,max(BOUNCECOUNT) AS BOUNCECOUNT,
CAMPAIGNS.NAME,max(CLICKCOUNT) AS CLICKCOUNT,max(UCLICKCOUNT) AS UCLICKCOUNT, MAX(RUN_DT) AS START_DT,CAMPAIGNS.CATEGORY,ACTIONID
 FROM sim_reporting_flowmetrics WITH (NOLOCK)
INNER JOIN CAMPAIGNS WITH (NOLOCK) ON sim_reporting_flowmetrics.CAMPAIGNID=CAMPAIGNS.ID AND campaigns.category in ('WEKA_ACTU','WEKA_COBRAND','WEKA_FID','WEKA_LB','WEKA_PRODUITS','WEKA_WEBCONF','WKJOBS_CANDIDATS','WKJOBS_RECRUTEURS') AND CAMPAIGNS.RUN_DT >= '2020-01-01'
group by campaignid, name,CAMPAIGNS.CATEGORY,ACTIONID) AS S
ON (T.CAMPAIGNID=S.CAMPAIGNID AND T.NOM_STAT='WEKA_EMAILING_CUMUL' AND T.ACTIONID=S.ACTIONID)
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (DT,CAMPAIGNID,NB_OUVERTURES,NOM_STAT,NB_U_OUVERTURES,NB_CIBLE,NB_DELIVRE,NB_BOUNCES,NOM_CAMPAGNE,NB_CLICS,NB_U_CLICS,START_DT,CAMPAGNE_CATEGORY,ACTIONID,UPDATE_DT)
		 VALUES (getdate(),S.CAMPAIGNID,S.VIEWCOUNT,'WEKA_EMAILING_CUMUL',S.UVIEWCOUNT,S.TARGETCOUNT,S.DELIVERYCOUNT,S.BOUNCECOUNT,
S.NAME,S.CLICKCOUNT,S.UCLICKCOUNT,S.START_DT,S.CATEGORY,S.ACTIONID,GETDATE())
	WHEN MATCHED
		THEN 
			UPDATE SET 
				T.DT=getdate(),
				T.NB_OUVERTURES=S.VIEWCOUNT,
				T.NB_U_OUVERTURES=S.UVIEWCOUNT,
				T.NB_CIBLE=S.TARGETCOUNT,
				T.NB_DELIVRE=S.DELIVERYCOUNT,
				T.NB_BOUNCES=S.BOUNCECOUNT,
				T.NOM_CAMPAGNE=S.NAME,
				T.NB_CLICS=S.CLICKCOUNT,
				T.NB_U_CLICS=S.UCLICKCOUNT,
				T.START_DT=S.START_DT,
				T.CAMPAGNE_CATEGORY=S.CATEGORY,
				T.ACTIONID=S.ACTIONID,
				T.UPDATE_DT=GETDATE();			   

UPDATE DATA_WEKA_STATS_DATASTUDIO
SET MAILID=(SELECT mailid FROM CAMPAIGN_ACTIONS t2 with (nolock) where DATA_WEKA_STATS_DATASTUDIO.campaignid=t2.campaignid and DATA_WEKA_STATS_DATASTUDIO.actionid=t2.actionid)
WHERE MAILID IS NULL AND NOM_STAT='WEKA_EMAILING_CUMUL' AND ACTIONID IS NOT NULL AND CAMPAIGNID IS NOT NULL; 

-- taux désabo 


UPDATE DATA_WEKA_STATS_DATASTUDIO
SET MAILID=(SELECT mailid FROM CAMPAIGN_ACTIONS t2 with (nolock) where DATA_WEKA_STATS_DATASTUDIO.campaignid=t2.campaignid and DATA_WEKA_STATS_DATASTUDIO.actionid=t2.actionid)
WHERE MAILID IS NULL AND NOM_STAT='WEKA_EMAILING_CUMUL' AND ACTIONID IS NOT NULL AND CAMPAIGNID IS NOT NULL; 
	   
MERGE INTO DATA_WEKA_STATS_DATASTUDIO AS T
USING (
	select t1.campaignid,t2.mailid,count(*) AS NB_CLICS,count(distinct(userid)) AS NB_U_CLICS,t2.ACTIONID AS ACTIONID  FROM flags t1
	with (nolock)
	inner join CAMPAIGN_ACTIONS t2 with (nolock) on t1.campaignid=t2.campaignid and t1.actionid=t2.actionid
	and t1.probeid>0
	where exists (select 1 from campaigns with (nolock) where campaigns.category in ('WEKA_ACTU','WEKA_COBRAND','WEKA_FID','WEKA_LB','WEKA_PRODUITS','WEKA_WEBCONF','WKJOBS_CANDIDATS','WKJOBS_RECRUTEURS') AND t1.campaignid=campaigns.id and campaigns.run_dt>=getdate()-15)
	and exists (select 1 from MAIL_PROBES t3 with (nolock) where t2.mailid=t3.mailid and t1.probeid=t3.probeid AND t3.CATEGORY ='OPTOUT')
	group by t1.campaignid,t2.mailid,t2.ACTIONID
) AS S
ON (T.CAMPAIGNID=S.CAMPAIGNID  AND T.MAILID=S.MAILID AND T.NOM_STAT='WEKA_EMAILING_CUMUL' AND T.ACTIONID=S.ACTIONID) 
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (campaignid,mailid,nb_optout,nb_u_optout,nom_stat,update_dt,actionid)
		 VALUES (S.CAMPAIGNID,S.mailid,S.nb_clics,S.nb_u_clics,'WEKA_EMAILING_CUMUL',getdate(),S.ACTIONID)
	WHEN MATCHED
		THEN 
			UPDATE SET 
		t.nb_optout=S.nb_clics,
		t.nb_u_optout=S.nb_u_clics,
		t.update_dt=getdate();


-- plainte et spam
-- pb sur les actions id


UPDATE DATA_WEKA_STATS_DATASTUDIO
SET 
NB_SPAM=(SELECT COUNT(*) FROM MTA_DELIVERYINFO T1 WITH (NOLOCK) 
inner join CAMPAIGN_ACTIONS t2 with (nolock) on t1.campaignid=t2.campaignid and t1.actionid=t2.actionid
WHERE T1.CAMPAIGNID=DATA_WEKA_STATS_DATASTUDIO.CAMPAIGNID AND T1.STATE =26 AND T2.ACTIONID=DATA_WEKA_STATS_DATASTUDIO.ACTIONID),
NB_PLAINTE=(SELECT COUNT(*) FROM MTA_DELIVERYINFO T1 WITH (NOLOCK) 
inner join CAMPAIGN_ACTIONS t2 with (nolock) on t1.campaignid=t2.campaignid and t1.actionid=t2.actionid
WHERE T1.CAMPAIGNID=DATA_WEKA_STATS_DATASTUDIO.CAMPAIGNID AND T1.STATE =27 AND T2.ACTIONID=DATA_WEKA_STATS_DATASTUDIO.ACTIONID)
WHERE (NB_SPAM IS NULL OR NB_PLAINTE IS NULL) AND NOM_STAT = 'WEKA_EMAILING_CUMUL'




UPDATE DATA_WEKA_STATS_DATASTUDIO 
SET NOM_EMAIL=(select name from MAILS with (nolock) where MAILS.id=DATA_WEKA_STATS_DATASTUDIO.mailid)
WHERE NOM_EMAIL IS NULL AND MAILID IS NOT NULL;

UPDATE DATA_WEKA_STATS_DATASTUDIO 
SET ACTION_NAME=(select name from CAMPAIGN_ACTIONS t2 with (nolock) where t2.actionid=DATA_WEKA_STATS_DATASTUDIO.actionid and t2.campaignid=DATA_WEKA_STATS_DATASTUDIO.campaignid)
WHERE ACTION_NAME IS NULL AND actionid is not null and campaignid is not null;




--- fin des stats journeys et emails 

MERGE INTO DATA_WEKA_STATS_DATASTUDIO AS T
USING (
SELECT CAMPAIGNID,max(VIEWCOUNT) AS VIEWCOUNT,max(UVIEWCOUNT) AS UVIEWCOUNT,max(TARGETCOUNT) AS TARGETCOUNT,max(DELIVERYCOUNT) AS DELIVERYCOUNT,max(BOUNCECOUNT) AS BOUNCECOUNT,
CAMPAIGNS.NAME,max(CLICKCOUNT) AS CLICKCOUNT,max(UCLICKCOUNT) AS UCLICKCOUNT, MAX(RUN_DT) AS START_DT,CAMPAIGNS.CATEGORY 
 FROM sim_reporting_flowmetrics WITH (NOLOCK)
INNER JOIN CAMPAIGNS WITH (NOLOCK) ON sim_reporting_flowmetrics.CAMPAIGNID=CAMPAIGNS.ID AND campaigns.category in ('WEKA_ACTU','WEKA_COBRAND','WEKA_FID','WEKA_LB','WEKA_PRODUITS','WEKA_WEBCONF','WKJOBS_CANDIDATS','WKJOBS_RECRUTEURS') AND CAMPAIGNS.RUN_DT >= '2020-01-01'
group by campaignid, name,CAMPAIGNS.CATEGORY) AS S
ON (T.CAMPAIGNID=S.CAMPAIGNID AND T.NOM_STAT='WEKA_EMAILING_CUMUL')
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (DT,CAMPAIGNID,NB_OUVERTURES,NOM_STAT,NB_U_OUVERTURES,NB_CIBLE,NB_DELIVRE,NB_BOUNCES,NOM_CAMPAGNE,NB_CLICS,NB_U_CLICS,START_DT,CAMPAGNE_CATEGORY)
		 VALUES (getdate(),S.CAMPAIGNID,S.VIEWCOUNT,'WEKA_EMAILING_CUMUL',S.UVIEWCOUNT,S.TARGETCOUNT,S.DELIVERYCOUNT,S.BOUNCECOUNT,
S.NAME,S.CLICKCOUNT,S.UCLICKCOUNT,S.START_DT,S.CATEGORY)
	WHEN MATCHED
		THEN 
			UPDATE SET 
				T.DT=getdate(),
				T.NB_OUVERTURES=S.VIEWCOUNT,
				T.NB_U_OUVERTURES=S.UVIEWCOUNT,
				T.NB_CIBLE=S.TARGETCOUNT,
				T.NB_DELIVRE=S.DELIVERYCOUNT,
				T.NB_BOUNCES=S.BOUNCECOUNT,
				T.NOM_CAMPAGNE=S.NAME,
				T.NB_CLICS=S.CLICKCOUNT,
				T.NB_U_CLICS=S.UCLICKCOUNT,
				T.START_DT=S.START_DT,
				T.CAMPAGNE_CATEGORY=S.CATEGORY;			   


--- stats par lien 

MERGE INTO DATA_WEKA_STATS_DATASTUDIO AS T
USING (
	select t1.campaignid,t1.probeid,t2.mailid,left(t3.name,100) AS NAME,left(t3.url,100) AS URL,count(*) AS NB_CLICS,count(distinct(userid)) AS NB_U_CLICS FROM flags t1
	with (nolock)
	inner join CAMPAIGN_ACTIONS t2 with (nolock) on t1.campaignid=t2.campaignid and t1.actionid=t2.actionid
	inner join MAIL_PROBES t3 with (nolock) on t2.mailid=t3.mailid and t1.probeid=t3.probeid
	and t1.probeid>0
	where exists (select 1 from campaigns with (nolock) where campaigns.category in ('WEKA_ACTU','WEKA_COBRAND','WEKA_FID','WEKA_LB','WEKA_PRODUITS','WEKA_WEBCONF','WKJOBS_CANDIDATS','WKJOBS_RECRUTEURS') AND t1.campaignid=campaigns.id and campaigns.run_dt>=getdate()-15)
	group by t1.campaignid,t1.probeid,t2.mailid,t3.name,t3.url
) AS S
ON (T.CAMPAIGNID=S.CAMPAIGNID AND T.PROBEID=S.PROBEID AND T.MAILID=S.MAILID AND T.NOM_STAT='WEKA_EMAILING_PROBES_CUMUL') 
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (campaignid,probeid,mailid,probe_name,probe_url,nb_clics,nb_u_clics,nom_stat,update_dt)
		 VALUES (S.CAMPAIGNID,S.probeid,S.mailid,s.name,S.url,S.nb_clics,S.nb_u_clics,'WEKA_EMAILING_PROBES_CUMUL',getdate())
	WHEN MATCHED
		THEN 
			UPDATE SET 
		t.probe_name=s.name,
		t.probe_url=S.url,
		t.nb_clics=S.nb_clics,
		t.nb_u_clics=S.nb_u_clics,
		t.update_dt=getdate();
		
		

UPDATE DATA_WEKA_STATS_DATASTUDIO 
SET CAMPAGNE_CATEGORY=(select category from campaigns with (nolock) where campaigns.id=DATA_WEKA_STATS_DATASTUDIO.campaignid)
WHERE CAMPAGNE_CATEGORY IS NULL;

UPDATE DATA_WEKA_STATS_DATASTUDIO 
SET NOM_CAMPAGNE=(select name from campaigns with (nolock) where campaigns.id=DATA_WEKA_STATS_DATASTUDIO.campaignid)
WHERE NOM_CAMPAGNE IS NULL;


UPDATE DATA_WEKA_STATS_DATASTUDIO 
SET NOM_EMAIL=(select name from MAILS with (nolock) where MAILS.id=DATA_WEKA_STATS_DATASTUDIO.mailid)
WHERE NOM_EMAIL IS NULL;

UPDATE DATA_WEKA_STATS_DATASTUDIO 
SET ACTION_NAME=(select name from CAMPAIGN_ACTIONS t2 with (nolock) where t2.id=DATA_WEKA_STATS_DATASTUDIO.actionid)
WHERE ACTION_NAME IS NULL AND actionid is not null;




UPDATE DATA_WEKA_STATS_DATASTUDIO 
SET START_DT=(select run_dt from campaigns with (nolock) where campaigns.id=DATA_WEKA_STATS_DATASTUDIO.campaignid)
WHERE START_DT IS NULL;

 -- export des données 
 select ID,NOM_STAT,convert(varchar, DT, 23) AS DT,CAMPAIGNID,NB_OUVERTURES,NB_U_OUVERTURES,NB_CIBLE,NB_DELIVRE,NB_BOUNCES,NOM_CAMPAGNE,NB_CLICS,NB_U_CLICS,convert(varchar, START_DT, 23) AS START_DT,convert(varchar, UPDATE_DT, 23) AS UPDATE_DT,MAILID,PROBEID,PROBE_NAME,PROBE_URL,NOM_EMAIL,CAMPAGNE_CATEGORY,PROBE_CATEGORY,REPLACE(TX_CLIC,',','.') AS TX_CLIC,REPLACE(TX_CLIC_UNIQUE,',','.')  AS TX_CLIC_UNIQUE,ACTIONID,ACTION_NAME,NB_OPTOUT,NB_U_OPTOUT,NB_SPAM,NB_PLAINTEfrom DATA_WEKA_STATS_DATASTUDIO;