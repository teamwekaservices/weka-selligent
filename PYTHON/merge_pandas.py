#merge des fichiers
import pandas as pd
import datatable as dt 
import time
start_time = time.time()

#on traite le fichier de réferentiel des APE 
f2=pd.read_csv("./int_courts_naf_rev_2.csv", sep=";",low_memory=False)
#f2=dt.fread("./int_courts_naf_rev_2.csv")
#f2=f2.to_pandas()

#on lit le fichier des établissements 
f1=pd.read_csv("./StockUniteLegale_utf8", sep=",",low_memory=False)
#f1=dt.fread("./StockUniteLegale_utf8")
#f1=f1.to_pandas()

f = pd.merge(f1,f2,left_on='activitePrincipaleUniteLegale', right_on='code', how='inner')
                   
f.to_csv("stockUniteLegaleActif_pandas.csv", index=False)

print("--- %s seconds ---" % (time.time() - start_time))
