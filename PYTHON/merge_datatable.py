#merge des fichiers
import pandas as pd
import datatable as dt 
import time
start_time = time.time()

#on traite le fichier de réferentiel des APE 
#f2=pd.read_csv("./int_courts_naf_rev_2.csv", sep=";",low_memory=False)
f2=dt.fread("./int_courts_naf_rev_2.csv")

#on lit le fichier des établissements 
#f1=pd.read_csv("./StockUniteLegale_utf8", sep=",",low_memory=False)
f1=dt.fread("./StockUniteLegale_utf8 2.csv")

f = pd.merge(f1,f2,left_on='activitePrincipaleUniteLegale', right_on='code', how='inner')
f3=f1.merge(f2, on="x", how="left")
                   
f.to_csv("stockUniteLegaleActif_datatable.csv", index=False)

print("--- %s seconds ---" % (time.time() - start_time))
