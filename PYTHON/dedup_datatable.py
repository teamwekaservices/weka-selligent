#traitement du fichier des etablissements a garder

# Filter Cols
import pandas as pd

f=pd.read_csv("./StockUniteLegale_utf8.csv", sep=",", low_memory=False,dtype = {'siren': str,'statutDiffusionUniteLegale': str,'unitePurgeeUniteLegale': str,'sigleUniteLegale': str,'sexeUniteLegale': str,'prenom1UniteLegale': str,'prenom2UniteLegale': str,'prenom3UniteLegale': str,'prenom4UniteLegale': str,'prenomUsuelUniteLegale': str,'pseudonymeUniteLegale': str,'identifiantAssociationUniteLegale': str,'trancheEffectifsUniteLegale': str,'anneeEffectifsUniteLegale': str,'dateDernierTraitementUniteLegale': str,'nombrePeriodesUniteLegale': str,'categorieEntreprise': str,'anneeCategorieEntreprise': str,'dateDebut': str,'etatAdministratifUniteLegale': str,'nomUniteLegale': str,'nomUsageUniteLegale': str,'denominationUniteLegale': str,'denominationUsuelle1UniteLegale': str,'denominationUsuelle2UniteLegale': str,'denominationUsuelle3UniteLegale': str,'categorieJuridiqueUniteLegale': str,'activitePrincipaleUniteLegale': str,'nomenclatureActivitePrincipaleUniteLegale': str,'nicSiegeUniteLegale': str,'economieSocialeSolidaireUniteLegale': str,'caractereEmployeurUniteLegale': str}, usecols=['siren','sigleUniteLegale','etatAdministratifUniteLegale','denominationUniteLegale','categorieJuridiqueUniteLegale','activitePrincipaleUniteLegale','nicSiegeUniteLegale'])



f = pd.merge(f, f2, 
                   left_on='activitePrincipaleUniteLegale', 
                   right_on='code',
                   how='inner')

# On ne garde que les etablissement actifs
f = f[(f["etatAdministratifUniteLegale"] == 'A')]

#on ne garde que les établissements dans certaines catégories juridiques 
A = ['3210','4110','4120','4130','4140','4150','7112','7113','7120','7160','7171','7172','7179','7210','7220','7225','7229','7230','7312','7313','7314','7343','7344','7345','7346','7347','7348','7349','7353','7357','7361','7364','7365','7366','7367','7371','7371','7378','7384']
f = f[(f["categorieJuridiqueUniteLegale"].isin(A))]
# on filtre les colonnes
keep_col = ['siren','sigleUniteLegale','etatAdministratifUniteLegale','denominationUniteLegale','categorieJuridiqueUniteLegale','activitePrincipaleUniteLegale','nicSiegeUniteLegale','libelle']
new_f = f[keep_col]

new_f.to_csv("stockUniteLegaleActif.csv", index=False)


#traitement des unites legales 
#f=pd.read_csv("./StockUniteLegale_utf8.csv", sep=",", low_memory=False,dtype = {'siren': str,'etatAdministratifUniteLegale': str,'denominationUniteLegale': str,'categorieJuridiqueUniteLegale': str}, usecols=['siren','etatAdministratifUniteLegale','denominationUniteLegale','categorieJuridiqueUniteLegale'])

# On ne garde que les unites actives et qui ne sont pas entrepreneur individuel (1000)
#f = f[(f["etatAdministratifUniteLegale"] == 'A') & (f["categorieJuridiqueUniteLegale"] != '1000')]


# on filtre les colonnes
#keep_col = ['siren','etatAdministratifUniteLegale','denominationUniteLegale','categorieJuridiqueUniteLegale']
#new_f = f[keep_col]

#new_f.to_csv("stockUniteLegalActifSans1000.csv", index=False)



#merge des 2 fichiers 
#dataset_1 = pd.read_csv('stock6920Z_Etablissement.csv')
#dataset_2 = pd.read_csv('stockUniteLegalActifSans1000.csv')
    
#new_dataset = dataset_1.merge(dataset_2, how='inner', on='siren')
#new_dataset.to_csv("stockFinal6920Z.csv", index=False)
