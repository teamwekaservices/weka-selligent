import pandas as pd
import requests
from bs4 import BeautifulSoup
import certifi

# root = 'https://www.foph.fr/oph/Les-Offices/#|t=lesOfficesHomeLivetable&p=1&l=100&s=doc.title&d=asc'
root = 'https://www.foph.fr/oph/Les-Offices/'
website = f'{root}'
result = requests.get(website, verify=False)
content = result.text
soup = BeautifulSoup(content,'lxml')

box = soup.find('div', id_='xwikicontent')

table = soup.find('table', class_='xwiki-livetable-display')
tbody = table.find('tbody', class_='xwiki-livetable-display-body')

test = tbody.find('td')

print(test)

print(tbody.get_text())

# for element in tbody.find_all('tr'):
#     print(element)