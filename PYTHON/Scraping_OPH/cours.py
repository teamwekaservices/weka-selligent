import requests
from bs4 import BeautifulSoup

# result = requests.get("www.google.com")

# content = result.text

# soup = BeautifulSoup(content, "lxml")

# soup.find(id="specific_id")

# soup.find('tag', class_="class_name")

# soup.find('article', class_="main_article")

# soup.fin('h1')

# soup.find_all("h2") #return list

# URL="https://subslikescript.com/movie/Titanic-120338"

# result = requests.get(URL)
# content = result.text

# soup = BeautifulSoup(content, 'lxml')

# # print(soup.prettify())

# box =soup.find('article', class_='main-article')

# title = box.find('h1').get_text()
# #transcript = box.find('div', class_='full-script').get_text(strip=True, separator=' ')
# transcript = box.find('p', class_='plot').get_text()

# # print(title)

# # print(transcript)

# with open(f'{title}.txt', 'w') as file: # title + '.txt
#     file.write(transcript)

# Scraping multiple link in the same page

# root = 'https://subslikescript.com'
# website = f'{root}/movies'
# result = requests.get(website)
# content = result.text
# soup = BeautifulSoup(content,'lxml')

# box = soup.find('article', class_='main-article') 

# movies = []
# for element in box.find_all('a', href=True):
#     movies.append(element['href'])
#     print(element['title'])

# for movie in movies:
#     website = f'{root}/{movie}'
#     result = requests.get(website)
#     content = result.text
#     soup = BeautifulSoup(content,'lxml')
#     box = soup.find('article', class_='main-article')

#     title = box.find('h1').get_text()
#     transcript = box.find('h1').get_text()

#     with open(f'{title}.txt', 'w') as file: # title + '.txt
#         file.write(transcript)


# Scraping multiple page

root = 'https://subslikescript.com'
website = f'{root}/movies_letter-A'
result = requests.get(website)
content = result.text
soup = BeautifulSoup(content,'lxml')

#pagination
pagination =  soup.find('ul', class_='pagination')
pages = pagination.find_all('li', class_='page-item')
last_page = pages[-2].text

for page in range(1, int(last_page+1)):
    website = f'{root}/movies_letter_A?page = {page}'
    result = requests.get(website)
    content = result.text
    soup = BeautifulSoup(content, 'lxml')

box = soup.find('article', class_='main-article') 

movies = []
for element in box.find_all('a', href=True):
    movies.append(element['href'])
    print(element['title'])

for movie in movies:
    website = f'{root}/{movie}'
    result = requests.get(website)
    content = result.text
    soup = BeautifulSoup(content,'lxml')
    box = soup.find('article', class_='main-article')

    title = box.find('h1').get_text()
    transcript = box.find('h1').get_text()

    with open(f'{title}.txt', 'w', encoding='utf-8') as file: # title + '.txt
        file.write(transcript)