#merge des fichiers
import pandas as pd

#on traite le fichier de réferentiel des APE 
f2=pd.read_csv("./int_courts_naf_rev_2.csv", sep=";",low_memory=False)

#on lit le fichier des établissements 
f1=pd.read_csv("./stockUniteLegaleActif.csv", sep=",",low_memory=False)

f = pd.merge(f, f2, 
                   left_on='activitePrincipaleUniteLegale', 
                   right_on='code',
                   how='inner')
                   
f.to_csv("stockUniteLegaleActif.csv", index=False)
