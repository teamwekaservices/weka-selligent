CREATE OR REPLACE FUNCTION web_weka.wks_repousser_entite(table_destination character varying, table_a_repousser character varying, schema_table character varying, job_name character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
	declare 
		vstructure_table varchar;
		vstructure_req varchar;
		vLoad_id numeric;
		vBatch_id numeric;
		vReqSQL varchar;
	begin
		--On récupère les colonnes communes des table avec le load_id en plus de la table de destination, structure de la table avec les colonnes communes
		select string_agg(c1.column_name, ', ')
		Into vstructure_table
		from information_schema."columns" c1
		where table_name = table_destination
			and exists(
			select 1
			from information_schema."columns" c2
			where c1.column_name = c2.column_name
				and c2.table_name = table_a_repousser
				or c1.column_name = 'b_loadid');
			
		--On récupère les colonnes communes des table par rapport à la table à réinsérer, le nom des colonnes servant pour l'insert
		select string_agg(c1.column_name, ',') 
		Into vstructure_req
		from information_schema."columns" c1
		where table_name = table_a_repousser
			and exists(
			select 1
			from information_schema."columns" c2
			where c1.column_name = c2.column_name 
				and c2.table_name = table_destination);
			
		
		--On créée un nouveau chargement et on récupère le load_id
		vLoad_id := weka_repo.get_new_loadid (
				'WEKACustomers',
				'FCT_WK_PUSH_ENTITE',
				'Repousser des entités',
				'WKS_FCT_WK_PUSH_ENTITE'
			);
		
		--On construit notre requête pour repousser la table à repousser dans la table de destination
		vReqSQL := 'Insert into ' || schema_table || '.' || table_destination::varchar || ' (' || vstructure_table::varchar || ') Select ' || vLoad_id::varchar || ', ' || vstructure_req::varchar || ' From ' || schema_table || '.' || table_a_repousser::varchar;

		execute vReqSQL;
		
		--on submit le load et on envoie un message pour le confirmer, dans le cas contraire on affiche un message pour indiquer que la fonction à échoué
		vBatch_id := weka_repo.submit_load(
			vLoad_id,
			job_name,
			'WKS_FCT_WK_PUSH_ENTITE'
			);
		return 'La table a été repousser !!' || ' | load_id : ' || vLoad_id::varchar || ' | batch_id : ' || vBatch_id::varchar || ' | Dans la table : ' || schema_table || '.' || table_destination::varchar || ' | Si erreur dans le traitement du load : ' || schema_table || '.' || table_a_repousser::varchar;	
	END;
$function$
;
