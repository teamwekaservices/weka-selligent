CREATE OR REPLACE FUNCTION web_ti.alimentation_referentiel_entreprise()
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
	declare 
		v_req varchar;
		vLoad_id numeric;
		vBatch_id numeric;
	begin
						
			/* -- Création d'une table temporaire permettant de stocker les entreprises de la base Sirène --
			 * Les établissements sont insérés s'ils correspondent à un siret contenu dans un ti_ciam_profile et qu'ils ne sont pas déjà stockés dans l'entité ti_referentiel_entreprise
			 * Les établissements peuvent être des sièges dans ce cas ils sont insérés, ils peuvent être des établissements secondaires et dans ce cas il faut aussi insérer leur siège
			 * Le f_etab_siege est rempli seulement dans le cas ou l'établissement n'est pas un siège */
			create temporary table temp_ref_entre(
				code_etablissement Varchar(20), 
				raison_sociale Varchar(128), 
				code_societe Varchar(20), 
				siege bool, 
				commune Varchar(100), 
				etat_administratif Varchar(8), 
				code_commune Varchar(5), 
				latitude Varchar(20), 
				longitude Varchar(20), 
				code_postal Varchar(10), 
				adresse Varchar(128), 
				tranche_effectif Varchar(2), 
				code_dep_etablissement Varchar(3), 
				code_region_etab Varchar(3), 
				etat_administratif_etab Varchar(8), 
				enseigne_etab Varchar(50), 
				sigle Varchar(25), 
				f_etab_siege Varchar(30),
				f_codes_ape Varchar(5)
			) on commit drop;
		
		
			/* -- Insertion des établissements siège --
			 * Select -> On récupère les établissements sièges (siret = siret_siege_unite_legale), ils doivent se trouver dans la table des profiles et ne pas être déja présent dans le référentiel des entreprise
			 * Il faut que le siret du profile ne soit pas nul, possède 14 caractères et que le f_country du profile soit 'FR'
			 * Même restriction pour le siret et f_code_pays du referentiel
			 * On insère ensuite ces établissements dans la table temporaire */
			insert into temp_ref_entre (code_etablissement, raison_sociale, code_societe , siege, commune, etat_administratif, code_commune, latitude, longitude, code_postal, adresse, tranche_effectif, code_dep_etablissement, code_region_etab, etat_administratif_etab, enseigne_etab, sigle, f_codes_ape)
			select bs.siret, 
	        	bs.denomination_unite_legale, 
	        	bs.siren, 
	        	case bs.etablissement_siege when 'oui' then true else false end, 
	        	bs.libelle_commune_etablissement, 
	        	bs.etat_administratif_unite_legale, 
	        	bs.code_commune_etablissement, 
	        	bs.latitude, 
	        	bs.longitude, 
	        	bs.code_postal_etablissement, 
	        	bs.adresse_etablissement, 
	        	case bs.tranche_effectifs_etablissement_triable::varchar when '-1'::varchar then 'NN'::varchar when '0'::varchar then '00'::varchar when '1'::varchar then '01'::varchar when '2'::varchar then '02'::varchar when '3'::varchar then '03'::varchar end, 
	        	bs.code_departement_etablissement, 
	        	bs.code_region_etablissement,
	        	bs.etat_administratif_etablissement, 
	        	bs.enseigne_etablissement, 
	        	bs.sigle_unite_legale, 
				case length(replace(bs.activite_principale_etablissement, '.', '')) when 5 then replace(bs.activite_principale_etablissement, '.', '') else null end
					from weka_ext.base_sirene bs
					where siret = siret_siege_unite_legale
						and exists( 
							select 1
							from weka_mdm.gd_ti_ciam_profile gtcp
							where gtcp.siret is not null
								and bs.siret = gtcp.siret 
								and gtcp.f_country in ('FR', 'GF', 'RE', 'MQ', 'PM', 'BL', 'MF', 'TF', 'WF', 'PF', 'NC', 'GP', 'RE', 'YT')
								and length(gtcp.siret) = 14
								and not exists (
									select 1 
									from weka_mdm.gd_ti_referentiel_entreprise gre
									where gtcp.siret = gre.code_etablissement
										and length(gre.code_etablissement) = 14));
			
									
			/* -- Insertion des établisements siège reliés a des établssements secondaires --
			 * Select -> On récupère les etablissements qui sont reliés a des établissement secondaires (liste siret siège pour les établissements secondaires), ces établissements secondaires doivent se trouver dans la table des profiles et ne pas être déja présent dans le référentiel des entreprise
			 * Il faut que le siret du profile ne soit pas nul, possède 14 caractères et que le f_country du profile soit 'FR'
			 * Même restriction pour le siret et f_code_pays du referentiel
			 * On insère ensuite ces établissements dans la table temporaire */						
			insert into temp_ref_entre (code_etablissement, raison_sociale, code_societe , siege, commune, etat_administratif, code_commune, latitude, longitude, code_postal, adresse, tranche_effectif, code_dep_etablissement, code_region_etab, etat_administratif_etab, enseigne_etab, sigle, f_codes_ape)
			select  bs1.siret, 
				bs1.denomination_unite_legale,
				bs1.siren, 
				case bs1.etablissement_siege when 'oui' then true else false end, 
				bs1.libelle_commune_etablissement, 
				bs1.etat_administratif_unite_legale, 
				bs1.code_commune_etablissement, 
				bs1.latitude, 
				bs1.longitude, 
				bs1.code_postal_etablissement, 
				bs1.adresse_etablissement, 
				case bs1.tranche_effectifs_etablissement_triable::varchar when '-1'::varchar then 'NN'::varchar when '0'::varchar then '00'::varchar when '1'::varchar then '01'::varchar when '2'::varchar then '02'::varchar when '3'::varchar then '03'::varchar end,  
				bs1.code_departement_etablissement,
				bs1.code_region_etablissement, 
				bs1.etat_administratif_etablissement, 
				bs1.enseigne_etablissement, 
				bs1.sigle_unite_legale, 
				case length(replace(bs1.activite_principale_etablissement, '.', '')) when 5 then replace(bs1.activite_principale_etablissement, '.', '') else null end
			from weka_ext.base_sirene bs1
				where siret in ( 
					select bs.siret_siege_unite_legale 
					from weka_ext.base_sirene bs
					where bs.siret not in (bs.siret_siege_unite_legale)
						and exists( 
							select 1
							from weka_mdm.gd_ti_ciam_profile gtcp
							where gtcp.siret is not null
								and bs.siret = gtcp.siret 
								and gtcp.f_country in ('FR', 'GF', 'RE', 'MQ', 'PM', 'BL', 'MF', 'TF', 'WF', 'PF', 'NC', 'GP', 'RE', 'YT')
								and length(gtcp.siret) = 14
								and not exists (
									select 1 
									from weka_mdm.gd_ti_referentiel_entreprise gre
									where gtcp.siret = gre.code_etablissement 
										and length(gre.code_etablissement) = 14)));

									
			/* -- Insertion des établisements secondaires --
			 * Select -> On récupère les établissements secondaires (siret != siret_siege_unite_legale), ils doivent se trouver dans la table des profils et ne pas être déjà présent dans le référentiel des entreprises 
			 * Il faut que le siret du profile ne soit pas nul, possède 14 caractères et que le f_country du profile soit 'FR'
			 * Même restriction pour le siret et f_code_pays du référentiel
			 * On insère ensuite ces établissements dans la table temporaire */								
			insert into temp_ref_entre (code_etablissement, raison_sociale, code_societe , siege, commune, etat_administratif, code_commune, latitude, longitude, code_postal, adresse, tranche_effectif, code_dep_etablissement, code_region_etab, etat_administratif_etab, enseigne_etab, sigle, f_codes_ape, f_etab_siege)					
			select bs.siret, 
				bs.denomination_unite_legale, 
				bs.siren, 
				case bs.etablissement_siege when 'oui' then true else false end, 
				bs.libelle_commune_etablissement, 
				bs.etat_administratif_unite_legale, 
				bs.code_commune_etablissement, 
				bs.latitude, 
				bs.longitude, 
				bs.code_postal_etablissement, 
				bs.adresse_etablissement, 
				case bs.tranche_effectifs_etablissement_triable::varchar when '-1'::varchar then 'NN'::varchar when '0'::varchar then '00'::varchar when '1'::varchar then '01'::varchar when '2'::varchar then '02'::varchar when '3'::varchar then '03'::varchar end, 
				bs.code_departement_etablissement, 
				bs.code_region_etablissement, 
				bs.etat_administratif_etablissement, 
				bs.enseigne_etablissement, 
				bs.sigle_unite_legale, 
				case length(replace(bs.activite_principale_etablissement, '.', '')) when 5 then replace(bs.activite_principale_etablissement, '.', '') else null end,
				concat('FR-', bs.siret_siege_unite_legale)
			from weka_ext.base_sirene bs
				where siret != siret_siege_unite_legale
						and exists( 
							select 1
							from weka_mdm.gd_ti_ciam_profile gtcp
							where gtcp.siret is not null
								and bs.siret = gtcp.siret 
								and gtcp.f_country in ('FR', 'GF', 'RE', 'MQ', 'PM', 'BL', 'MF', 'TF', 'WF', 'PF', 'NC', 'GP', 'RE', 'YT')
								and length(gtcp.siret) = 14
								and not exists (
									select 1 
									from weka_mdm.gd_ti_referentiel_entreprise gre
									where gtcp.siret = gre.code_etablissement
										and length(gre.code_etablissement) = 14));													
			
			-- Requête permettant d'avoir plus d'informations sur les enregistrements présent dans la table temporaire
			/*for v_record in select distinct code_etablissement, 
					raison_sociale, 
					code_societe , 
					siege, 
					commune,
					etat_administratif, 
					code_commune, 
					latitude, 
					longitude, 
					code_postal, 
					adresse, 
					tranche_effectif, 
					code_dep_etablissement,
					code_region_etab,
					etat_administratif_etab, 
					enseigne_etab, 
					sigle, 
					f_etab_siege,
					f_codes_ape 
				from temp_ref_entre 
				order by f_etab_siege desc
			loop
				v_req = concat(v_req, v_record.raison_sociale,' | ', v_record.code_societe,' | ', v_record.siege,' | ', v_record.commune,' | ', v_record.etat_administratif,' | ', v_record.code_commune,' | ', v_record.latitude,' | ', v_record.longitude,' | ', v_record.code_postal,' | ', v_record.adresse,' | ', v_record.tranche_effectif,' | ', v_record.code_dep_etablissement,' | ', v_record.code_region_etab,' | ', v_record.etat_administratif_etab,' | ', v_record.enseigne_etab,' | ', v_record.sigle,' | ', v_record.f_etab_siege,' | ', v_record.f_codes_ape,E'\n');
			end loop;*/
			
		if (select count(*) from temp_ref_entre) > 0 then
		
			/*
			 * On crée un chargement pour le traitement de l'insertion des données de la table temporaire dans le référentiel
			 */
			vLoad_id := weka_repo.get_new_loadid (
	  			'WEKACustomers',
	            'TI_JOB_175',
	            'Insertion et MAJ des entreprises pour TI',
	           	'JOB175_REF_ENT_TI'
	        );
			
	        /* 
	         * On insère les enregistrements contenus dans la table temporaire dans le référentiel des entreprises
	         */
			insert into weka_mdm.sa_ti_referentiel_entreprise (b_loadid, ti_referentiel_entreprise, b_classname, b_creator, b_credate, code_etablissement, raison_sociale, code_societe , siege, commune, etat_administratif, code_commune, latitude, longitude, code_postal, adresse, tranche_effectif, code_dep_etablissement, code_region_etab, etat_administratif_etab, enseigne_etab, sigle, f_etab_siege, f_codes_ape)	
			select distinct vLoad_id, 
	        	concat('FR-', code_etablissement),
	        	'tiReferentielEntreprise', 
	        	'JOB175_REF_ENT_TI',
	        	now(),
				code_etablissement, 
				raison_sociale, 
				code_societe , 
				siege, 
				commune,
				etat_administratif, 
				code_commune, 
				latitude, 
				longitude, 
				code_postal, 
				adresse, 
				tranche_effectif, 
				code_dep_etablissement,
				code_region_etab,
				etat_administratif_etab, 
				enseigne_etab, 
				sigle, 
				f_etab_siege,
				f_codes_ape 
			from temp_ref_entre 
			order by f_etab_siege desc;
			
		
			/*
			 * On valide et on soumet le chargement créer précédemment avec son load_id
			 */
			vBatch_id := weka_repo.submit_load(
	            vLoad_id,
	            'DAJ_AuthorreferentielEntreprises', 
	            'JOB175_REF_ENT_TI'
	       	);
			
			-- On retourne un message avec le load_id et le batch_id du chargement utilisables pour vérifier le status du chargement	
			v_req = concat('Load_id:',vLoad_id,',Batch_id:',vBatch_id);
		else
			v_req = 'Il n''y a pas de ligne à traiter.';
		end if;
	
		return v_req;
		
		
	END;
$function$
;
