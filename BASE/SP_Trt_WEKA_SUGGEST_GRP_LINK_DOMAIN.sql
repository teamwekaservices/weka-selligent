CREATE OR REPLACE FUNCTION web_weka.wk_suggest_groups_link()
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
	declare 
		V_nb_dom_pot numeric := 
		(select count(distinct domain_email)
		from weka_mdm.gd_wk_ciam_profile prof
		where prof.domain_email is not null
			and prof.f_wk_ciam_group is not null
			and not exists(
				select 1
				from weka_mdm.gd_wk_ciam_email_domain_gen emailgen
				where prof.domain_email = emailgen.email_domain 
			)
			and not exists(
				select 1
				from weka_mdm.gd_wk_ciam_group_link grplink
				where grplink."type" = 'emailDomain'
					and prof.domain_email = grplink.valeur
			));
				
		vLoad_id numeric;
		vBatch_id numeric;
	begin
		if V_nb_dom_pot > 0 then
			vLoad_id := weka_repo.get_new_loadid (
				'WEKACustomers',
				'FCT_WK_SUGGEST_GROUPS_LINK',
				'Suggestion de groupes link via les domaines',
				'WKS_FCT_SUGGEST_GROUPS_LINKS'
			);
			
			insert into weka_mdm.sa_wk_ciam_group_link (b_loadid, wk_ciam_group_link_id, b_classname, b_credate, b_creator, "type", valeur, f_wk_ciam_group, validated) 
				(select vLoad_id, nextval('weka_mdm.seq_wk_ciam_group_link'), 'wkCiamGroupLink',  now(), 'FCT_WK_SUGGEST_GROUPS_LINKS', 'emailDomain', prof.domain_email, prof.f_wk_ciam_group, false 
				from weka_mdm.gd_wk_ciam_profile prof
				where prof.domain_email is not null
					and prof.f_wk_ciam_group is not null
					and not exists(
					select 1
					from weka_mdm.gd_wk_ciam_email_domain_gen emailgen
					where prof.domain_email = emailgen.email_domain 
					)
					and not exists(
					select 1
					from weka_mdm.gd_wk_ciam_group_link grplink
					where grplink."type" = 'emailDomain'
						and prof.domain_email = grplink.valeur
					)
					and not exists(
					select 1
					from weka_mdm.gx_wk_ciam_group_link gxgrplink
					where gxgrplink."type" = 'emailDomain'
						and prof.domain_email = gxgrplink.valeur
					)
				group by prof.domain_email, prof.f_wk_ciam_group);
				
			vBatch_id := weka_repo.submit_load(
				vLoad_id,
				'DAJ_AuthorwkCiamGroupLinks',
				'WKS_FCT_SUGGEST_GROUPS_LINKS'
			);
		
		
			return concat('Soumission du load : ', vLoad_id, ' avec ', V_nb_dom_pot, ' groupes link');
		elseif V_nb_dom_pot = 0 then
			return 'Aucun groupe link détecté';
		end if;
	END;
$function$
;
