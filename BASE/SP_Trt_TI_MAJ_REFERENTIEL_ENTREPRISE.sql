CREATE OR REPLACE FUNCTION web_ti.maj_referentiel_entreprise()
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
	declare 
		v_req varchar;
		vLoad_id numeric;
		vBatch_id numeric;
	BEGIN
		/* -- Création d'une table temporaire permettant de stocker les entreprises de la base Sirène --
		 * Les établissements sont insérés s'ils correspondent à un siret contenu dans un ti_ciam_profile et qu'ils ne sont pas déjà stockés dans l'entité ti_referentiel_entreprise
		 * Les établissements peuvent être des sièges dans ce cas ils sont insérés, ils peuvent être des établissements secondaires et dans ce cas il faut aussi insérer leur siège
		 * Le f_etab_siege est rempli seulement dans le cas ou l'établissement n'est pas un siège */
		create temporary table temp_ref_entre_up(
			code_etablissement Varchar(20), 
			raison_sociale Varchar(128), 
			code_societe Varchar(20), 
			siege bool, 
			commune Varchar(100), 
			etat_administratif Varchar(8), 
			code_commune Varchar(5), 
			latitude Varchar(20), 
			longitude Varchar(20), 
			code_postal Varchar(10), 
			adresse Varchar(128), 
			tranche_effectif Varchar(2), 
			code_dep_etablissement Varchar(3), 
			code_region_etab Varchar(3), 
			etat_administratif_etab Varchar(8), 
			enseigne_etab Varchar(50), 
			sigle Varchar(25), 
			f_etab_siege Varchar(30),
			f_codes_ape Varchar(5)
		) on commit drop;
		
		insert into temp_ref_entre_up (code_etablissement, raison_sociale, code_societe , siege, commune, etat_administratif, code_commune, latitude, longitude, code_postal, adresse, tranche_effectif, code_dep_etablissement, code_region_etab, etat_administratif_etab, enseigne_etab, sigle, f_codes_ape, f_etab_siege)					
		select bs.siret, 
			bs.denomination_unite_legale, 
			bs.siren, 
			case bs.etablissement_siege when 'oui' then true else false end as "siege", 
			bs.libelle_commune_etablissement, 	
			bs.etat_administratif_unite_legale, 		
			bs.code_commune_etablissement, 
			bs.latitude, 
			bs.longitude, 
			bs.code_postal_etablissement, 
			bs.adresse_etablissement, 
			case bs.tranche_effectifs_etablissement_triable::varchar when '-1'::varchar then 'NN'::varchar when '0'::varchar then '00'::varchar when '1'::varchar then '01'::varchar when '2'::varchar then '02'::varchar when '3'::varchar then '03'::varchar end as "tranche_effectif", 
			bs.code_departement_etablissement, 
			bs.code_region_etablissement, 
			bs.etat_administratif_etablissement, 
			bs.enseigne_etablissement, 
			bs.sigle_unite_legale, 		
			case length(replace(bs.activite_principale_etablissement, '.', '')) when 5 then replace(bs.activite_principale_etablissement, '.', '') else null end,
			concat('FR-', bs.siret_siege_unite_legale) as "siret_siege"
		from weka_mdm.gd_ti_referentiel_entreprise gtre 
		join weka_ext.base_sirene bs on gtre.code_etablissement = bs.siret
		where bs.date_maj = now()::date;	
		
		if (select count(*) from temp_ref_entre_up) > 0 then
		
			/*
			 * On crée un chargement pour le traitement de l'insertion des données de la table temporaire dans le référentiel
			 */
			vLoad_id := weka_repo.get_new_loadid (
	  			'WEKACustomers',
	            'TI_JOB_174',
	            'MAJ des entreprises pour TI',
	           	'JOB174_WKS_ALIM_SIRENE'
	        );
			
	        /* 
	         * On insère les enregistrements contenus dans la table temporaire dans le référentiel des entreprises
	         */
			insert into weka_mdm.sa_ti_referentiel_entreprise (b_loadid, ti_referentiel_entreprise, b_classname, b_creator, b_credate, code_etablissement, raison_sociale, code_societe , siege, commune, etat_administratif, code_commune, latitude, longitude, code_postal, adresse, tranche_effectif, code_dep_etablissement, code_region_etab, etat_administratif_etab, enseigne_etab, sigle, f_etab_siege, f_codes_ape)	
			select distinct vLoad_id, 
	        	concat('FR-', code_etablissement),
	        	'tiReferentielEntreprise', 
	        	'JOB174_WKS_ALIM_SIRENE',
	        	now(),
				code_etablissement, 
				raison_sociale, 
				code_societe , 
				siege, 
				commune,
				etat_administratif, 
				code_commune, 
				latitude, 
				longitude, 
				code_postal, 
				adresse, 
				tranche_effectif, 
				code_dep_etablissement,
				code_region_etab,
				etat_administratif_etab, 
				enseigne_etab, 
				sigle, 
				f_etab_siege,
				f_codes_ape 
			from temp_ref_entre_up 
			order by f_etab_siege desc;
			
		
			/*
			 * On valide et on soumet le chargement créer précédemment avec son load_id
			 */
			vBatch_id := weka_repo.submit_load(
	            vLoad_id,
	            'DAJ_AuthorreferentielEntreprises', 
	            'JOB174_WKS_ALIM_SIRENE'
	       	);
			
			-- On retourne un message avec le load_id et le batch_id du chargement utilisables pour vérifier le status du chargement	
			v_req = concat('Load_id:',vLoad_id,',Batch_id:',vBatch_id);
		else
			v_req = 'Il n''y a pas de ligne à traiter.';
		end if;
	
		return v_req;
	END;
$function$
;
