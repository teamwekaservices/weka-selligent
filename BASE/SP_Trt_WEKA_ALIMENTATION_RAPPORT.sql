CREATE OR REPLACE FUNCTION web_weka.wk_alimentation_rapport_ciam()
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
	declare
		v_result record;
		v_string varchar;
		v_qte numeric = 0;
		v_req_part varchar;
		v_partition record;
	begin
		
		if (SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'web_weka' AND tablename  = 'weka_data_metrics_'||date_part('year',current_date))) = false then
			v_req_part = 'CREATE TABLE web_weka.weka_data_metrics_' || (select date_part('year',current_date)) || ' PARTITION OF web_weka.weka_data_metrics for values from ((date_trunc(''year'', current_date::date))::date) to ((date_trunc(''year'', current_date::date) + interval ''1 year'' - interval ''1 day'')::date)';
			
			execute v_req_part;
		
			v_string = 'Création de partition pour l''année ' || (select date_part('year',current_date)) || E' \n';
		end if;
		---------
		
		--Rapport 1 - Prio 1
		--Requete 1
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select 
		'NB_PROFIL' as nom_valeur,
		null as valeur_1,
		null as valeur_2,
		count(*) as qt_total,
		sum(case 
			when gwcp.SELL_MASTER_EDITO = true then 1
			else 0
		end) as qt_master_edito,
		sum(case 
			when gwcp.SELL_MASTER_WEBCONF = true then 1
			else 0
		end) as qt_master_webconf,
		sum(case 
			when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
			else 0
		end) as qt_master_info_produit,
		sum(case 
			when gwcp.SELL_INFOS_PARTENAIRES = true then 1
			else 0
		end) as qt_master_info_partenaire,
		sum(case 
			when gwcp.SELL_OPTOUT > 0 then 1
			else 0
		end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp;
	
		--requete 2
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select
		'NB_PROFIL_FONCTION' as nom_valeur,
		f_fonction as valeur_1,
		null as valeur_2,
		count(*) as qt_total,
		sum(case 
			when gwcp.SELL_MASTER_EDITO = true then 1
			else 0
		end) as qt_master_edito,
		sum(case 
			when gwcp.SELL_MASTER_WEBCONF = true then 1
			else 0
		end) as qt_master_webconf,
		sum(case 
			when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
			else 0
		end) as qt_master_info_produit,
		sum(case 
			when gwcp.SELL_INFOS_PARTENAIRES = true then 1
			else 0
		end) as qt_master_info_partenaire,
		sum(case 
			when gwcp.SELL_OPTOUT > 0 then 1
			else 0
		end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp 
		group by 1,3,2;
		
		
		-- Requete 3
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select 
		'NB_PROFIL_SERVICE' as nom_valeur,
		f_service  as valeur_1,
		null as valeur_2,
		count(*) as qt_total,
		sum(case 
			when gwcp.SELL_MASTER_EDITO = true then 1
			else 0
		end) as qt_master_edito,
		sum(case 
			when gwcp.SELL_MASTER_WEBCONF = true then 1
			else 0
		end) as qt_master_webconf,
		sum(case 
			when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
			else 0
		end) as qt_master_info_produit,
		sum(case 
			when gwcp.SELL_INFOS_PARTENAIRES = true then 1
			else 0
		end) as qt_master_info_partenaire,
		sum(case 
			when gwcp.SELL_OPTOUT > 0 then 1
			else 0
		end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp 
		group by 1,2,3;
		
		-- Requete 4
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select 
		'NB_PROFIL_ETABLISSEMENT' as nom_valeur,
		profile_type  as valeur_1,
		null as valeur_2,
		count(*) as qt_total,
		sum(case 
			when gwcp.SELL_MASTER_EDITO = true then 1
			else 0
		end) as qt_master_edito,
		sum(case 
			when gwcp.SELL_MASTER_WEBCONF = true then 1
			else 0
		end) as qt_master_webconf,
		sum(case 
			when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
			else 0
		end) as qt_master_info_produit,
		sum(case 
			when gwcp.SELL_INFOS_PARTENAIRES = true then 1
			else 0
		end) as qt_master_info_partenaire,
		sum(case 
			when gwcp.SELL_OPTOUT > 0 then 1
			else 0
		end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp 
		group by 1,2,3;
		
		-- Requete 5
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select 
		'NB_PROFIL_SECTEUR_ACTIVITE' as nom_valeur,
		f_secteur_activite  as valeur_1, 
		null as valeur_2,
		count(*) as qt_total,
		sum(case 
			when gwcp.SELL_MASTER_EDITO = true then 1
			else 0
		end) as qt_master_edito,
		sum(case 
			when gwcp.SELL_MASTER_WEBCONF = true then 1
			else 0
		end) as qt_master_webconf,
		sum(case 
			when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
			else 0
		end) as qt_master_info_produit,
		sum(case 
			when gwcp.SELL_INFOS_PARTENAIRES = true then 1
			else 0
		end) as qt_master_info_partenaire,
		sum(case 
			when gwcp.SELL_OPTOUT > 0 then 1
			else 0
		end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp 
		group by 1,2,3;
		
		--Rapport 6 - Prio 2
		--Requete 1
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select 
		'DERNIERE_ACTIVITE_DT_TRANCHE',
		case
			when sell_derniere_activite_dt::date >= current_date - interval '6 months' then '0-6mois' 
			when sell_derniere_activite_dt::date < current_date - interval '6 months' and creation_date::date >= current_date - interval '12 months' then '6-12mois' 
			when sell_derniere_activite_dt::date < current_date - interval '12 months' and creation_date::date >= current_date - interval '18 months' then '12-18mois' 
			when sell_derniere_activite_dt::date is null then 'non calculée' 
			else '+18mois'
		end as valeur_1,
		null as valeur_2,
		count(*) as qt_totale,
		sum(case 
			when gwcp.SELL_MASTER_EDITO = true then 1
			else 0
		end) as qt_master_edito,
		sum(case 
			when gwcp.SELL_MASTER_WEBCONF = true then 1
			else 0
		end) as qt_master_webconf,
		sum(case 
			when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
			else 0
		end) as qt_master_info_produit,
		sum(case 
			when gwcp.SELL_INFOS_PARTENAIRES = true then 1
			else 0
		end) as qt_master_info_partenaire,
		sum(case 
			when gwcp.SELL_OPTOUT > 0 then 1
			else 0
		end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp 
		group by 1,2,3;
		
		-- Requete 2
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select 
		'NB_PROFIL_CREES_SEMAINE' as nom_valeur,
		null as valeur_1,
		null as valeur_2,
		count(*) as qt_total,
		sum(case 
			when gwcp.SELL_MASTER_EDITO = true then 1
			else 0
		end) as qt_master_edito,
		sum(case 
			when gwcp.SELL_MASTER_WEBCONF = true then 1
			else 0
		end) as qt_master_webconf,
		sum(case 
			when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
			else 0
		end) as qt_master_info_produit,
		sum(case 
			when gwcp.SELL_INFOS_PARTENAIRES = true then 1
			else 0
		end) as qt_master_info_partenaire,
		sum(case 
			when gwcp.SELL_OPTOUT > 0 then 1
			else 0
		end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp 
		where b_credate::date > current_date - interval '1 week';
		
		-- Requete 3
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select 'ANCIENNETE_BDD_PAR_ANNEE',
		DATE_PART('year', current_date) - DATE_PART('year', creation_date) as valeur_1,
		null as valeur_2,
		count(*) as qt_total,
		sum(case 
			when gwcp.SELL_MASTER_EDITO = true then 1
			else 0
		end) as qt_master_edito,
		sum(case 
			when gwcp.SELL_MASTER_WEBCONF = true then 1
			else 0
		end) as qt_master_webconf,
		sum(case 
			when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
			else 0
		end) as qt_master_info_produit,
		sum(case 
			when gwcp.SELL_INFOS_PARTENAIRES = true then 1
			else 0
		end) as qt_master_info_partenaire,
		sum(case 
			when gwcp.SELL_OPTOUT > 0 then 1
			else 0
		end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp 
		group by 1,2,3;
		
		-- Requete 4
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select 'ANCIENNETE_BDD_MOYENNE' as nom_valeur,
		avg(DATE_PART('year', current_date) - DATE_PART('year', creation_date)) as valeur_1,
		null as valeur_2,
		null as qt_total,
		sum(case 
			when gwcp.SELL_MASTER_EDITO = true then 1
			else 0
		end) as qt_master_edito,
		sum(case 
			when gwcp.SELL_MASTER_WEBCONF = true then 1
			else 0
		end) as qt_master_webconf,
		sum(case 
			when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
			else 0
		end) as qt_master_info_produit,
		sum(case 
			when gwcp.SELL_INFOS_PARTENAIRES = true then 1
			else 0
		end) as qt_master_info_partenaire,
		sum(case 
			when gwcp.SELL_OPTOUT > 0 then 1
			else 0
		end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp;
		
		
		-- Rapport 7 - Prio 3
		-- Requete 1
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select 
		'NB_NOUVEAUX_UTILISATEURS_SEMAINE' as nom_valeur,
		role as valeur_1,
		date_part('week', gwcar.b_credate::timestamp) as valeur_2,
		count(*) as qt_total,
		sum(case 
			when gwcp.SELL_MASTER_EDITO = true then 1
			else 0
		end) as qt_master_edito,
		sum(case 
			when gwcp.SELL_MASTER_WEBCONF = true then 1
			else 0
		end) as qt_master_webconf,
		sum(case 
			when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
			else 0
		end) as qt_master_info_produit,
		sum(case 
			when gwcp.SELL_INFOS_PARTENAIRES = true then 1
			else 0
		end) as qt_master_info_partenaire,
		sum(case 
			when gwcp.SELL_OPTOUT > 0 then 1
			else 0
		end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciamprofile_accnt_role gwcar
		join weka_mdm.gd_wk_ciam_profile gwcp on gwcar.f_profile_id = gwcp.profile_id 
		where gwcar.b_credate::date > current_date - interval '1 year'
		group by 1,2,3;
		
		--Requete 2
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select 
		'NON_CONNECTE_DEPUIS_6_MOIS' as nom_valeur,
		gwcar."role" as valeur_1,
		(DATE_PART('year', current_date) - DATE_PART('year', gwcp.last_authentication)) * 12 + (DATE_PART('month', current_date) - DATE_PART('month', gwcp.last_authentication)) as valeur_2,
		count(*) as qt_totale,
		sum(case 
			when gwcp.SELL_MASTER_EDITO = true then 1
			else 0
		end) as qt_master_edito,
		sum(case 
			when gwcp.SELL_MASTER_WEBCONF = true then 1
			else 0
		end) as qt_master_webconf,
		sum(case 
			when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
			else 0
		end) as qt_master_info_produit,
		sum(case 
			when gwcp.SELL_INFOS_PARTENAIRES = true then 1
			else 0
		end) as qt_master_info_partenaire,
		sum(case 
			when gwcp.SELL_OPTOUT > 0 then 1
			else 0
		end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp
		inner join weka_mdm.gd_wk_ciamprofile_accnt_role gwcar on gwcar.f_profile_id = gwcp.profile_id 
		where 
		gwcp.last_authentication::date < current_date - interval '6 months'
		and gwcp.last_authentication::date >= current_date - interval '1 year'
		and exists (
			select 1 from weka_mdm.gd_wk_ciam_accnt_sub gwcas 
			where gwcas.f_account_id = gwcar.f_account_id and gwcas.status = 'ACTIF'
		)
		group by 1,2,3;
	
	
		-- Rapport 2 - Prio 4
		-- Requete 1
		/*insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select 
			'NB_PROFIL_ROLE' as nom_valeur,
			gwcar."role"  as valeur_1,
			null as valeur_2,
			count(*) as qt_total,
			sum(case 
				when gwcp.SELL_MASTER_EDITO = true then 1
				else 0
			end) as qt_master_edito,
			sum(case 
				when gwcp.SELL_MASTER_WEBCONF = true then 1
				else 0
			end) as qt_master_webconf,
			sum(case 
				when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
				else 0
			end) as qt_master_info_produit,
			sum(case 
				when gwcp.SELL_INFOS_PARTENAIRES = true then 1
				else 0
			end) as qt_master_info_partenaire,
			sum(case 
				when gwcp.SELL_OPTOUT > 0 then 1
				else 0
			end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp 
		inner join weka_mdm.gd_wk_ciamprofile_accnt_role gwcar on gwcar.f_profile_id = gwcp.profile_id 
		inner join  weka_mdm.gd_wk_ciam_accnt_sub gwcas on gwcar.f_account_id  = gwcas.f_account_id
		where gwcas.status = 'ACTIF'
		group by 1,2;*/
	
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select 
			'NB_PROFIL_ROLE' as nom_valeur,
			gwcar."role"  as valeur_1,
			null as valeur_2,
			count(distinct profile_id) as qt_total,
			count(distinct(
				case 
					when gwcp.SELL_MASTER_EDITO = true then profile_id 
			end)) as qt_master_edito,
			count(distinct(
				case 
					when gwcp.SELL_MASTER_WEBCONF = true then profile_id 
			end)) as qt_master_webconf,
			count(distinct(
				case 
					when gwcp.SELL_MASTER_INFO_PRODUIT = true then profile_id 
			end)) as qt_master_info_produit,
			count(distinct(
				case 
					when gwcp.SELL_INFOS_PARTENAIRES = true then profile_id 
			end)) as qt_master_info_partenaire,
			count(distinct(
				case 
					when gwcp.SELL_OPTOUT > 0 then profile_id 
			end)) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp 
		inner join weka_mdm.gd_wk_ciamprofile_accnt_role gwcar on gwcar.f_profile_id = gwcp.profile_id 
		inner join  weka_mdm.gd_wk_ciam_accnt_sub gwcas on gwcar.f_account_id  = gwcas.f_account_id
		where gwcas.status = 'ACTIF'
		group by 1,2;
	
		--Requete 2
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select
			'NB_PROFIL_ROLE' as nom_valeur,
			'PROSPECT' as valeur_1,
			null as valeur_2,
			count(*) as qt_total,
			sum(case
				when gwcp.SELL_MASTER_EDITO = true then 1
				else 0
			end) as qt_master_edito,
			sum(case
				when gwcp.SELL_MASTER_WEBCONF = true then 1
				else 0
			end) as qt_master_webconf,
			sum(case
				when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
				else 0
			end) as qt_master_info_produit,
			sum(case
				when gwcp.SELL_INFOS_PARTENAIRES = true then 1
				else 0
			end) as qt_master_info_partenaire,
			sum(case
				when gwcp.SELL_OPTOUT > 0 then 1
				else 0
			end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp
		where
			silhouette = false
			and not exists (
				select 1 from weka_mdm.gd_wk_ciamprofile_accnt_role gwcar
				inner join weka_mdm.gd_wk_ciam_accnt_sub gwcas on gwcar.f_account_id = gwcas.f_account_id
				where gwcas.status = 'ACTIF' and gwcar.f_profile_id = gwcp.profile_id
			);
		
		
		
		--Requete 3
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select
			'NB_PROFIL_ROLE' as nom_valeur,
			'SILHOUETTE' as valeur_1,
			null as valeur_2,
			count(*) as qt_total,
			sum(case
				when gwcp.SELL_MASTER_EDITO = true then 1
				else 0
			end) as qt_master_edito,
			sum(case
				when gwcp.SELL_MASTER_WEBCONF = true then 1
				else 0
			end) as qt_master_webconf,
			sum(case
				when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
				else 0
			end) as qt_master_info_produit,
			sum(case
				when gwcp.SELL_INFOS_PARTENAIRES = true then 1
				else 0
			end) as qt_master_info_partenaire,
			sum(case
				when gwcp.SELL_OPTOUT > 0 then 1
				else 0
			end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp
		where silhouette = true;
		
	
		-- Rapport 5 - Prio 5
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,valeur_3,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select
			'NB_PROFIL_CIBLE' as nom_valeur,
			null as valeur_1,
			null as valeur_2,
			null as valeur_3,
			count(*) as qt_total,
			sum(case
				when gwcp.SELL_MASTER_EDITO = true then 1
				else 0
			end) as qt_master_edito,
			sum(case
				when gwcp.SELL_MASTER_WEBCONF = true then 1
				else 0
			end) as qt_master_webconf,
			sum(case
				when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
				else 0
			end) as qt_master_info_produit,
			sum(case
				when gwcp.SELL_INFOS_PARTENAIRES = true then 1
				else 0
			end) as qt_master_info_partenaire,
			sum(case
				when gwcp.SELL_OPTOUT > 0 then 1
				else 0
			end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp
		where sell_delivery_last_dt::date >= current_date -7;
	
		-- Rapport 4 - Prio 6
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,valeur_3,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select
			'NB_LEAD' as nom_valeur,
			gwcl.utm_campaign as valeur_1,
			gwcl.utm_medium as valeur_2,
			gwcl.source_form as valeur_3,
			count(*) as qt_total,
			sum(case
				when gwcp.SELL_MASTER_EDITO = true then 1
				else 0
			end) as qt_master_edito,
			sum(case
				when gwcp.SELL_MASTER_WEBCONF = true then 1
				else 0
			end) as qt_master_webconf,
			sum(case
				when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
				else 0
			end) as qt_master_info_produit,
			sum(case
				when gwcp.SELL_INFOS_PARTENAIRES = true then 1
				else 0
			end) as qt_master_info_partenaire,
			sum(case
				when gwcp.SELL_OPTOUT > 0 then 1
				else 0
			end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp
		inner join weka_mdm.gd_wk_ciam_lead gwcl on gwcl.f_profile_id = gwcp.profile_id
		where
			gwcl.creation_date::date >= current_date-7
		group by 1,2,3,4;
	
		--Rapport 3 - Prio 7
		insert into web_weka.weka_data_metrics (nom_valeur,valeur_1,valeur_2,qt_total,qt_master_edito,qt_master_webconf,qt_master_info_produit,qt_master_info_partenaire,qt_optout_selligent)
		select
			'NB_PROFIL_CONSENTEMENT' as nom_valeur,
			gwcp.domain_email as valeur_1,
			case
				when gwcedg.email_domain_generique_id is not null then 'OUI'
				else 'NON'
			end as valeur_2,
			count(*) as qt_total,
			sum(case
				when gwcp.SELL_MASTER_EDITO = true then 1
				else 0
			end) as qt_master_edito,
			sum(case
				when gwcp.SELL_MASTER_WEBCONF = true then 1
				else 0
			end) as qt_master_webconf,
			sum(case
				when gwcp.SELL_MASTER_INFO_PRODUIT = true then 1
				else 0
			end) as qt_master_info_produit,
			sum(case
				when gwcp.SELL_INFOS_PARTENAIRES = true then 1
				else 0
			end) as qt_master_info_partenaire,
			sum(case
				when gwcp.SELL_OPTOUT > 0 then 1
				else 0
			end) as qt_optout_selligent
		from weka_mdm.gd_wk_ciam_profile gwcp
		left join weka_mdm.gd_wk_ciam_email_domain_gen gwcedg on gwcp.domain_email = gwcedg.email_domain
		group by 1,2,3
		order by qt_total desc
		limit 200;

		---------
		for v_result in (select nom_valeur, count(*) as qte from web_weka.weka_data_metrics where creation_date = current_date group by nom_valeur)
		loop
			v_string = concat(v_string,E'\n','Pour le rapport du ',v_result.nom_valeur,': ',v_result.qte,' lignes ont été insérées.');
			v_qte = v_result.qte + v_qte;
		end loop;
		
		v_string = concat(v_string,E'\n \n-- Total nb lignes insérées: ', v_qte,' --');
		
		return v_string;
	END;
$function$
;
