CREATE OR REPLACE FUNCTION web_weka.wk_repousser_profiles()
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
	declare 
		vcount_donnee integer;
		vstructure_table varchar;
		vstructure_req varchar;
		vLoad_id numeric;
		vBatch_id numeric;
		vReqSQL varchar;
	begin
		select count(profile_id)
		into vcount_donnee
		from weka_mdm.gd_wk_ciam_profile prof
		where f_wk_ciam_group is null
			and (exists(
				select 1
				from weka_mdm.gd_wk_ciam_group_link grplink
				where grplink."type" = 'emailDomain'
					and prof.domain_email = grplink.valeur
					and validated = true
			)
			or exists(
				select 1
				from weka_mdm.gd_wk_ciam_group_link grplink
				where grplink."type" = 'SIREN'
					and prof.siren  = grplink.valeur
					and validated = true));
		
		--Faire vérification si donnée a poussée
		if vcount_donnee > 0 then 
			--On récupère les colonnes communes des table avec le load_id en plus de la table de destination, structure de la table avec les colonnes communes
			select string_agg(c1.column_name, ', ')
			Into vstructure_table
			from information_schema."columns" c1
			where table_name = 'sa_wk_ciam_profile'
				and exists(
				select 1
				from information_schema."columns" c2
				where c1.column_name = c2.column_name
					and c2.table_name = 'gd_wk_ciam_profile'
					or c1.column_name = 'b_loadid');
				
			--On récupère les colonnes communes des table par rapport à la table à réinsérer, le nom des colonnes servant pour l'insert
			select string_agg(c1.column_name, ',') 
			Into vstructure_req
			from information_schema."columns" c1
			where table_name = 'gd_wk_ciam_profile'
				and exists(
				select 1
				from information_schema."columns" c2
				where c1.column_name = c2.column_name 
					and c2.table_name = 'sa_wk_ciam_profile');			
				
			--On créée un nouveau chargement et on récupère le load_id
			vLoad_id := weka_repo.get_new_loadid (
					'WEKACustomers',
					'FCT_WK_PUSH_PROFILE',
					'Repousser les profiles étant reliés au groupe link',
					'WKS_FCT_WK_PUSH_PROFILE'
				);
			
			--On construit notre requête pour repousser la table à repousser dans la table de destination
			vReqSQL := 'Insert into weka_mdm.sa_wk_ciam_profile (' || vstructure_table::varchar || ') Select ' || vLoad_id::varchar || ', ' || vstructure_req::varchar || ' From weka_mdm.gd_wk_ciam_profile prof
	where f_wk_ciam_group is null
		and (exists(
			select 1
			from weka_mdm.gd_wk_ciam_group_link grplink
			where grplink."type" = ''emailDomain''
				and prof.domain_email = grplink.valeur
				and validated = true
		)
		or exists(
			select 1
			from weka_mdm.gd_wk_ciam_group_link grplink
			where grplink."type" = ''SIREN''
				and prof.siren  = grplink.valeur
				and validated = true
		))';
	
			execute vReqSQL;
			
			--on submit le load et on envoie un message pour le confirmer, dans le cas contraire on affiche un message pour indiquer que la fonction à échoué
			vBatch_id := weka_repo.submit_load(
				vLoad_id,
				'wkCiam',
				'WKS_FCT_WK_PUSH_PROFILE'
				);
			
			return 'La table profile a été repoussée !!' || ' | load_id : ' || vLoad_id::varchar || ' | batch_id : ' || vBatch_id::varchar;	
		else
			return 'Aucun profile à pousser a été trouvé!!';
		end if;
	END;
$function$
;
