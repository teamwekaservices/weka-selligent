AS
BEGIN
 /* ******************* Documentation Template (max 200 chars per line) ******************** 
  -- DID - Author: Loris COUTIN  
  -- DID - CreationDate: 10/04/2023 
  -- DID - UpdateDate: 04/07/2023 ajout traitrement : "STAGE", "PRODUCTS", "SOURCE_FORM"
  -- DID - Description: Sert à gérer les listes de valeurs dynamiques pour les utilisateurs.  
 ******************* Documentation Template (max 200 chars per line) ********************  */ 
SET NOCOUNT ON;

-- LiveStorm : TYPE_EVENT 
INSERT INTO OPTIONS_192 (CODE,FR,EN)
select DISTINCT TYPE_EVENT AS CODE,TYPE_EVENT AS FR, TYPE_EVENT AS EN from DATA_WEKA_WEBINAIRES_LIVE_STORM T1
WHERE NOT EXISTS (SELECT 1 FROM OPTIONS_192 T2 WHERE T1.TYPE_EVENT=T2.FR);

update T1
SET T1.TYPE_EVENT_CODE = T2.CODE 
FROM DATA_WEKA_WEBINAIRES_LIVE_STORM T1 
INNER JOIN OPTIONS_192 T2 ON T1.TYPE_EVENT = T2.FR
WHERE T1.TYPE_EVENT_CODE is null ;


-- LiveStorm : Titre Event 
INSERT INTO OPTIONS_193 (FR,EN,CODE)
select TITRE_EVENT AS FR, TITRE_EVENT AS EN,   (cast((select MAX(CODE) FROM OPTIONS_193) as int) + ROW_NUMBER() OVER (ORDER BY TITRE_EVENT) ) AS NEW_ID
from (select DISTINCT TITRE_EVENT, (select MAX(CODE) FROM OPTIONS_193) AS CODE from DATA_WEKA_WEBINAIRES_LIVE_STORM TA WHERE NOT EXISTS (SELECT 1 FROM OPTIONS_193 T2 WHERE TA.TITRE_EVENT=T2.FR)) T1

update T1
SET T1.TITRE_EVENT_CODE = T2.CODE 
FROM DATA_WEKA_WEBINAIRES_LIVE_STORM T1 
INNER JOIN OPTIONS_193 T2 ON T1.TITRE_EVENT = T2.FR
WHERE T1.TITRE_EVENT_CODE is null 

-- Offer template des factures 
INSERT INTO OPTIONS_196 (FR,EN,CODE)
SELECT DISTINCT value,value,value
FROM DATA_WEKA_FACTURE_SAP T1
CROSS APPLY STRING_SPLIT(OFFER_TEMPLATES, ',')
WHERE value NOT IN (select CODE from OPTIONS_196 );

-- Account Subscription Type 
-- suppression des données _ 
UPDATE DATA_WEKA_CIAM_ACCNT_SUB
SET SUBSCRIPTION_TYPE=REPLACE(SUBSCRIPTION_TYPE,'_','');
-- remplacement des données COMMERCIAL_TRIAL
UPDATE DATA_WEKA_CIAM_ACCNT_SUB
SET SUBSCRIPTION_TYPE=REPLACE(SUBSCRIPTION_TYPE,'COMMERCIALTRIAL','COMMTRIAL') WHERE SUBSCRIPTION_TYPE = 'COMMERCIALTRIAL';

-- Account Subscription Role 
update T1
SET T1.WK_ROLE_CODE = T2.CODE 
FROM DATA_WEKA_CIAM_PROFILE_ACCNT_ROLE T1 
INNER JOIN OPTIONS_200 T2 ON T1.WK_ROLE = T2.FR
WHERE T1.WK_ROLE_CODE is null ;

-- Offer template des WEKA_PRODUCT 
INSERT INTO OPTIONS_196 (FR,EN,CODE)
SELECT DISTINCT UPPER(T1.sku),UPPER(T1.sku),UPPER(T1.sku)
from DATA_WEKA_PRODUCT T1
where not exists (select 1 from OPTIONS_196 where code = UPPER(T1.sku))
AND T1.sku is not null
AND T1.sku <> ''


-- Titre produit 
INSERT INTO OPTIONS_201 (FR,EN,CODE)
select 
	TITLE AS FR, 
	TITLE AS EN,
	(CODE + ROW_NUMBER() OVER (ORDER BY TITLE) ) AS NEW_ID
from (
	select 
		DISTINCT TITLE, 
		(select MAX(CODE) FROM OPTIONS_201) AS CODE 
		from DATA_WEKA_PRODUCT TA 
		WHERE 
		NOT EXISTS (SELECT 1 FROM OPTIONS_201 T2 WHERE TA.TITLE=T2.FR)
) T1

-- mise à jour des titres produits 
UPDATE T1 
SET T1.TITRE_CODE = T2.CODE
FROM DATA_WEKA_PRODUCT T1
INNER JOIN OPTIONS_201 T2 ON T1.TITLE=T2.FR
WHERE T1.TITRE_CODE IS NULL

-- mise à jour de la liste des thématiques
INSERT INTO OPTIONS_202 (FR,EN,CODE)
SELECT THEMATIC_SLUG,THEMATIC_SLUG,THEMATIC_ID
FROM DATA_WEKA_THEMATIC T1 
WHERE NOT EXISTS (select 1 from OPTIONS_202 T2 WHERE T2.CODE = T1.THEMATIC_ID)


-- Leads : mise à jour des sources form
INSERT INTO OPTIONS_203 (FR,EN,CODE)
select 
	SOURCE_FORM AS FR, 
	SOURCE_FORM AS EN,
	(CODE + ROW_NUMBER() OVER (ORDER BY SOURCE_FORM) ) AS NEW_ID
from (
	select 
		DISTINCT SOURCE_FORM, 
		(select MAX(CODE) FROM OPTIONS_203) AS CODE 
		from DATA_WEKA_CIAM_LEAD TA 
		WHERE 
		NOT EXISTS (SELECT 1 FROM OPTIONS_203 T2 WHERE TA.SOURCE_FORM=T2.FR)
) T1		
ORDER BY SOURCE_FORM ASC;

update T1 
SET SOURCE_FORM_CODE = T2.CODE
FROM DATA_WEKA_CIAM_LEAD T1 
INNER JOIN OPTIONS_203 T2 ON T1.SOURCE_FORM=T2.FR
WHERE T1.SOURCE_FORM_CODE is null;

-- Leads : mise à jour des THEMATIC  
INSERT INTO OPTIONS_204 (FR,EN,CODE)
select 
	THEMATIC AS FR, 
	THEMATIC AS EN,
	(CODE + ROW_NUMBER() OVER (ORDER BY THEMATIC) ) AS NEW_ID
from (
	select 
		DISTINCT THEMATIC, 
		(select MAX(CODE) FROM OPTIONS_204) AS CODE 
		from DATA_WEKA_CIAM_LEAD TA 
		WHERE 
		NOT EXISTS (SELECT 1 FROM OPTIONS_204 T2 WHERE TA.THEMATIC=T2.FR) and TA.THEMATIC is not null
) T1		
ORDER BY THEMATIC ASC;

update T1 
SET THEMATIC_CODE = T2.CODE
FROM DATA_WEKA_CIAM_LEAD T1 
INNER JOIN OPTIONS_204 T2 ON T1.THEMATIC=T2.FR
WHERE T1.THEMATIC_CODE is null and THEMATIC IS NOT NULL;


-- Newsletter : mise à jour des TITLE
INSERT INTO OPTIONS_205 (FR,EN,CODE)
select 
	TITLE AS FR, 
	TITLE AS EN,
	(CODE + ROW_NUMBER() OVER (ORDER BY TITLE) ) AS NEW_ID
from (
	select 
		DISTINCT TITLE, 
		(select MAX(CODE) FROM OPTIONS_205) AS CODE 
		from DATA_WEKA_NEWSLETTER TA 
		WHERE 
		NOT EXISTS (SELECT 1 FROM OPTIONS_205 T2 WHERE TA.TITLE=T2.FR) and TA.TITLE is not null
) T1		
ORDER BY TITLE ASC;

update T1 
SET TITLE_CODE = T2.CODE
FROM DATA_WEKA_NEWSLETTER T1 
INNER JOIN OPTIONS_205 T2 ON T1.TITLE=T2.FR
WHERE T1.TITLE_CODE is null and TITLE IS NOT NULL;


-- calcul des données sur les accounts 
update T1
SET 
 T1.NB_PROFILES_ATTACHED=T2.nb_profiles,
 T1.NB_LICENCES_RESTANTES=T2.licences_restantes,
 T1.LICENCES_PERCENT=T2.user_percent
FROM DATA_WEKA_CIAM_ACCOUNT T1 
INNER JOIN (
select 
	account_id, 
	licence_count, 
	count(distinct gwcar.f_profile_id) as nb_profiles, 
	licence_count - count(distinct gwcar.f_profile_id) as licences_restantes,
	cast(round(cast(count(distinct gwcar.f_profile_id) as float)/cast(licence_count  as float)*100,0) as int) as user_percent
from DATA_WEKA_CIAM_ACCOUNT gwca 
inner join DATA_WEKA_CIAM_PROFILE_ACCNT_ROLE gwcar on gwca.account_id=gwcar.f_account_id  
where exists (select 1 from DATA_WEKA_CIAM_ACCNT_SUB gwcas where gwcas.f_account_id= gwca.account_id and gwcas.status='ACTIF' and SUBSCRIPTION_TYPE IN ('PAYING', 'NOTPAID') )
group by account_id, licence_count
having (licence_count - count(distinct gwcar.f_profile_id)) > 0) T2 on T1.account_id=T2.account_id;

-- calculs des données sur les produits abonnées d'une personne 
MERGE INTO DATA_WEKA_CIAM_DATA_CONSO AS TGT
USING (
    SELECT T1.PROFILE_ID, 
           STUFF((SELECT '|' + REPLACE(REPLACE(REPLACE(F_PRODUCT_ID, 'DT_FOLDER', ''), '{', ''), '}', '') 
                  FROM (
                      SELECT DISTINCT T3.F_PRODUCT_ID
                      FROM DATA_WEKA_CIAM_ACCNT_SUB T3 
                      INNER JOIN DATA_WEKA_CIAM_PROFILE_SUB T2 ON T2.F_ACCOUNT_SUBSCRIPTION_ID = T3.ACCOUNT_SUBSCRIPTION_ID 
                      WHERE T1.PROFILE_ID = T2.F_PROFILE_ID 
                      AND T3.SUBSCRIPTION_TYPE IN ('PAYING', 'NOTPAID') 
                      AND T3.STATUS = 'ACTIF' 
                      AND F_PRODUCT_ID LIKE '{DT_FOLDER}%'
                      GROUP BY T3.F_PRODUCT_ID -- ajouter cette clause GROUP BY
                  ) AS DistinctFProductID
                  FOR XML PATH('')), 1, 1, '') AS F_PRODUCT_ID_LIST
    FROM USERS_WEKA_EMAILS T1 
    GROUP BY T1.PROFILE_ID
) AS SRC (PROFILE_ID, F_PRODUCT_ID_LIST)
ON TGT.PROFILE_ID = SRC.PROFILE_ID
WHEN MATCHED AND SRC.F_PRODUCT_ID_LIST <> '' THEN 
    UPDATE SET TGT.PRODUITS_ABONNES = SRC.F_PRODUCT_ID_LIST
WHEN NOT MATCHED BY SOURCE THEN 
    UPDATE SET TGT.PRODUITS_ABONNES = ''
WHEN NOT MATCHED AND SRC.F_PRODUCT_ID_LIST <> '' THEN
    INSERT (PROFILE_ID, PRODUITS_ABONNES) VALUES (SRC.PROFILE_ID, SRC.F_PRODUCT_ID_LIST);



-----------------------------------------------------------------------------------------------------------------------------------------
----Trt : activity_state; TABLE : DATA_WK_ODOO_ACTIVITY
-----------------------------------------------------------------------------------------------------------------------------------------

--Insertion des nouvelles valeurs dans la LST "OPTIONS_212" via la colonne "activity_state" table "DATA_WK_ODOO_ACTIVITY"
INSERT INTO OPTIONS_212 (FR,EN,CODE)
select 
	activity_state AS FR, 
	activity_state AS EN,
	(CODE + ROW_NUMBER() OVER (ORDER BY activity_state) ) AS NEW_ID
from (
	select 
		DISTINCT activity_state, 
		(select MAX(CODE) from OPTIONS_212) as CODE 
		from Tmp_Weka_Odoo_Activity T2
		left join OPTIONS_212 T3 on T3.FR = T2.activity_state
		where T3.FR is null

) T1		
ORDER BY activity_state ASC;


-- MAJ de la colonne "ACTIVITY_STATE" de la table "DATA_WK_ODOO_ACTIVITY"
/*UPDATE DATA_WK_ODOO_ACTIVITY
SET ACTIVITY_STATE =(
SELECT CODE 
FROM OPTIONS_212 T1
INNER JOIN Tmp_Weka_Odoo_Activity T2 ON T1.FR = T2.ACTIVITY_STATE
WHERE DATA_WK_ODOO_ACTIVITY.wk_odoo_activity_id = T2.wk_odoo_activity_id)*/



-----------------------------------------------------------------------------------------------------------------------------------------
----Trt : activity_category ; TABLE : DATA_WK_ODOO_ACTIVITY
-----------------------------------------------------------------------------------------------------------------------------------------

--Insertion des nouvelles valeurs dans la LST "OPTIONS_212" via la colonne "activity_state" table "DATA_WK_ODOO_ACTIVITY"
INSERT INTO OPTIONS_213 (FR,EN,CODE)
select 
	activity_category  AS FR, 
	activity_category  AS EN,
	(CODE + ROW_NUMBER() OVER (ORDER BY activity_category ) ) AS NEW_ID
from (
	select 
		DISTINCT activity_category, 
		(select MAX(CODE) from OPTIONS_213) as CODE 
		from Tmp_Weka_Odoo_Activity T2
		left join OPTIONS_213 T3 on T3.FR = T2.activity_category
		where T3.FR is null

) T1		
ORDER BY activity_category  ASC;


-- MAJ de la colonne "ACTIVITY_STATE" de la table "DATA_WK_ODOO_ACTIVITY"
/*UPDATE DATA_WK_ODOO_ACTIVITY
SET activity_category =(
SELECT CODE 
FROM OPTIONS_213 T1
INNER JOIN Tmp_Weka_Odoo_Activity T2 ON T1.FR = T2.activity_category
WHERE DATA_WK_ODOO_ACTIVITY.wk_odoo_activity_id = T2.wk_odoo_activity_id)*/



-----------------------------------------------------------------------------------------------------------------------------------------
----Trt : STAGE; TABLE : Tmp_Weka_Odoo_Opportunity
-----------------------------------------------------------------------------------------------------------------------------------------

--Insertion des nouvelles valeurs dans la LST "OPTIONS_219" via la colonne "STAGE" table "Tmp_Weka_Odoo_Opportunity"
INSERT INTO OPTIONS_219 (FR,EN,CODE)
select 
	STAGE AS FR, 
	STAGE AS EN,
	(CODE + ROW_NUMBER() OVER (ORDER BY STAGE) ) AS NEW_ID
from (
	select 
		DISTINCT T2.STAGE, 
		(select MAX(CODE) from OPTIONS_219) as CODE 
		from Tmp_Weka_Odoo_Opportunity T2
		left join OPTIONS_219 T3 on T3.FR = T2.STAGE
		where T3.FR is null AND T2.STAGE is not null

) T1		
ORDER BY STAGE ASC;

-- MAJ de la colonne "STAGE" de la table "DATA_WEKA_ODOO_OPPORTUNITY"
UPDATE DATA_WEKA_ODOO_OPPORTUNITY
SET STAGE =(
SELECT CODE 
FROM OPTIONS_219 T1
INNER JOIN Tmp_Weka_Odoo_Opportunity T2 ON T1.FR = T2.STAGE
WHERE DATA_WEKA_ODOO_OPPORTUNITY.WK_ODOO_OPPORTUNITY_ID = T2.WK_ODOO_OPPORTUNITY_ID AND DATA_WEKA_ODOO_OPPORTUNITY.STAGE is null);


-----------------------------------------------------------------------------------------------------------------------------------------
-----Trt : PRODUCTS; TABLE : Tmp_Weka_Odoo_Opportunity
-----------------------------------------------------------------------------------------------------------------------------------------

--Insertion des nouvelles valeurs dans la LST "OPTIONS_218" via la colonne "PRODUCTS" table "Tmp_Weka_Odoo_Opportunity"
INSERT INTO OPTIONS_218 (FR,EN,CODE)
select 
	DecomposedValue AS FR, 
	DecomposedValue AS EN,
	(CODE + ROW_NUMBER() OVER (ORDER BY DecomposedValue) ) AS NEW_ID
from (
	SELECT 
	DISTINCT(s.value) AS DecomposedValue,
	(select MAX(CODE) from OPTIONS_218) as CODE
	FROM Tmp_Weka_Odoo_Opportunity T2
	CROSS APPLY STRING_SPLIT(T2.PRODUCTS, ',') s
	left join OPTIONS_218 T3 on T3.FR = s.value
	where T3.FR is null AND T2.PRODUCTS is not null

) T1		
ORDER BY NEW_ID ASC;

-- MAJ de la colonne "PRODUCTS" de la table "DATA_WEKA_ODOO_OPPORTUNITY" 
UPDATE DATA_WEKA_ODOO_OPPORTUNITY
SET PRODUCTS = 
(SELECT STRING_AGG(T2.CODE, ',')
FROM Tmp_Weka_Odoo_Opportunity T1
CROSS APPLY STRING_SPLIT(T1.PRODUCTS, ',') s
INNER JOIN OPTIONS_218 T2 ON T2.FR = s.value
WHERE T1.WK_ODOO_OPPORTUNITY_ID = DATA_WEKA_ODOO_OPPORTUNITY.WK_ODOO_OPPORTUNITY_ID
GROUP BY T1.WK_ODOO_OPPORTUNITY_ID);


-----------------------------------------------------------------------------------------------------------------------------------------
----Trt : SOURCE_FORM; TABLE : Tmp_Weka_Odoo_Opportunity
-----------------------------------------------------------------------------------------------------------------------------------------

--Insertion des nouvelles valeurs dans la LST "OPTIONS_203" via la colonne "SOURCE_FORM" de la table "Tmp_Weka_Odoo_Opportunity"
INSERT OPTIONS_203 (FR,EN,CODE)
SELECT 
SF AS FR,
SF AS EN,
(CODE + ROW_NUMBER() OVER (ORDER BY SF) ) AS NEW_ID
FROM
(
SELECT 
DISTINCT T2.SOURCE_FORM AS SF,
(SELECT MAX(CODE) FROM OPTIONS_203) AS CODE
FROM Tmp_Weka_Odoo_Opportunity T2
LEFT JOIN OPTIONS_203 T3 ON T2.SOURCE_FORM = T3.FR
WHERE T2.SOURCE_FORM is not null AND T3.FR is null
) T1;

-- MAJ de la colonne "SOURCE_FORM" de la table "DATA_WEKA_ODOO_OPPORTUNITY" 
UPDATE DATA_WEKA_ODOO_OPPORTUNITY
SET SOURCE_FORM = 
(SELECT CODE
FROM OPTIONS_203 T1
INNER JOIN Tmp_Weka_Odoo_Opportunity T2 ON T1.FR = T2.SOURCE_FORM
WHERE DATA_WEKA_ODOO_OPPORTUNITY.WK_ODOO_OPPORTUNITY_ID is null AND T2.WK_ODOO_OPPORTUNITY_ID = DATA_WEKA_ODOO_OPPORTUNITY.WK_ODOO_OPPORTUNITY_ID)

---- Modification champ dans la table WEKA_EMAILS ----
-- Alimentation du champ EST_CLIENT à true tout ceux qui ne sont pas à true et qui réponde à la requête permettant d'identifier des clients et s'il à besoin d'être modifier.
UPDATE USERS_WEKA_EMAILS SET EST_CLIENT = 1 Where  EXISTS (Select 1 from DATA_WEKA_CIAM_PROFILE_SUB T1 INNER JOIN DATA_WEKA_CIAM_ACCNT_SUB T2 ON T1.F_ACCOUNT_SUBSCRIPTION_ID=T2.ACCOUNT_SUBSCRIPTION_ID 
	WHERE VALID_TO>getdate() AND SUBSCRIPTION_TYPE in ('PAYING','NOTPAID') AND USERS_WEKA_EMAILS.PROFILE_ID = T1.F_PROFILE_ID) and (EST_CLIENT = 0 or EST_CLIENT IS NULL);
-- Alimentation du champ EST_CLIENT à false tout ceux qui ne sont pas à false et qui ne réponde pas à la requête permettant d'identifier des clients et s'il à besoin d'être modifier.
UPDATE USERS_WEKA_EMAILS SET EST_CLIENT = 0 Where NOT EXISTS (Select 1 from DATA_WEKA_CIAM_PROFILE_SUB T1 INNER JOIN DATA_WEKA_CIAM_ACCNT_SUB T2 ON T1.F_ACCOUNT_SUBSCRIPTION_ID=T2.ACCOUNT_SUBSCRIPTION_ID 
	WHERE VALID_TO>getdate() AND SUBSCRIPTION_TYPE in ('PAYING','NOTPAID') AND USERS_WEKA_EMAILS.PROFILE_ID = T1.F_PROFILE_ID) and (EST_CLIENT = 1 or EST_CLIENT IS NULL);


-----------------------------------------------------------------------------------------------------------------------------------------
----Trt : CHECK_ODOO; TABLE : DATA_WEKA_CIAM_LEAD
-----------------------------------------------------------------------------------------------------------------------------------------


UPDATE DATA_WEKA_CIAM_LEAD
SET CHECK_ODOO = 
    CASE 
        WHEN CHECK_ODOO_TXT = 'leadNonDecisionnaireToMkg' THEN 1
        WHEN CHECK_ODOO_TXT = 'otherRecentLead' THEN 2
        WHEN CHECK_ODOO_TXT = 'OK' THEN 3
        WHEN CHECK_ODOO_TXT = 'Déjà abonné' THEN 4
        WHEN CHECK_ODOO_TXT = 'multiLeadSameLoad' THEN 5
        WHEN CHECK_ODOO_TXT = 'leads non coeur de cible' THEN 6
        WHEN CHECK_ODOO_TXT = 'recentClosedOpportunity' THEN 7
        WHEN CHECK_ODOO_TXT = 'currentOpportunity' THEN 8
        WHEN CHECK_ODOO_TXT = 'HorsCible' THEN 9
        ELSE 0 -- Valeur par défaut si le texte ne correspond à aucun cas
    END
where check_odoo_txt is not null;






-----------------------------------------------------------------------------------------------------------------------------------------
----Trt : Mise à jour des THEMATIQUE_ID et DATE_DEBUT pour les données ARTICLES_WEKA_MASTERCLASSE_SUJET_QUIZ
-----------------------------------------------------------------------------------------------------------------------------------------

update ARTICLES_WEKA_MASTERCLASSE_SUJET_QUIZ 
SET ARTICLES_WEKA_MASTERCLASSE_SUJET_QUIZ.DATE_DEBUT=T2.DATE_DEBUT
FROM ARTICLES_WK_MASTERCLASS_CONTENT T2
where ARTICLES_WEKA_MASTERCLASSE_SUJET_QUIZ.DATE_DEBUT IS NULL
AND ARTICLES_WEKA_MASTERCLASSE_SUJET_QUIZ.ID_PARENT = T2.ID_SOURCE;


update ARTICLES_WEKA_MASTERCLASSE_SUJET_QUIZ 
SET ARTICLES_WEKA_MASTERCLASSE_SUJET_QUIZ.THEMATIQUE_ID=T2.THEMATIQUE_ID
FROM ARTICLES_WK_MASTERCLASS_CONTENT T2
where ARTICLES_WEKA_MASTERCLASSE_SUJET_QUIZ.THEMATIQUE_ID IS NULL
AND ARTICLES_WEKA_MASTERCLASSE_SUJET_QUIZ.ID_PARENT = T2.ID_SOURCE;


-----------------------------------------------------------------------------------------------------------------------------------------
---- Traitement DORMEUR_DT : https://redmine.weka-ssc.fr/issues/13246
-----------------------------------------------------------------------------------------------------------------------------------------

-- mise à jour du DORMEUR_DT si une activité dans la campagne EML_REACTIVATION_ÉTAPE_3_RETRAIT_LISTE_DIFFUSION la veille
update USERS_WEKA_EMAILS
set USERS_WEKA_EMAILS.DORMEUR_DT = actionqueue.sent_dt
from actionqueue  with (nolock)
inner join USERS_WEKA_EMAILS on USERS_WEKA_EMAILS.id=actionqueue.userid and DERNIERE_ACTIVITE_DT <= cast(DATEADD(DAY, -10, GETDATE()) as Date)
where CAST(sent_dt AS DATE) = cast(DATEADD(DAY, -1, GETDATE()) as Date) and actionqueue.campaignid = 16410

-- suppression du DORMEUR_DT si le contact a eu une activité dans la dernière année (nouveau clic, nouveau lead . .. ) 
update USERS_WEKA_EMAILS
set DORMEUR_DT=null
WHERE DERNIERE_ACTIVITE_DT > cast(DATEADD(YEAR, -1, GETDATE()) as Date) and DORMEUR_DT is not null 

END