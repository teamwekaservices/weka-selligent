AS
BEGIN
 /* ******************* Documentation Template (max 200 chars per line) ******************** 
  -- DID - Author: TB 
  -- DID - CreationDate: 01/11/2022
  -- DID - Version: 0.1.0 
  -- DID - Description: Description 
  -- DID - Exceptions: Exceptions 
  -- DID - BusinessRules: Rule 
  -- DID - LastModifiedBy: LastModifiedBy 
 ******************* Documentation Template (max 200 chars per line) ********************  */ 
SET NOCOUNT ON;


        -------------------------------------------------------------------------------------------------------------
        --  Procédure exécutée tous les jours et traite le nombre de clics pour chaque campagne par emails envoyés --
        --  L'export est ensuite geré par Talend FLUX 168                                                          --
        -------------------------------------------------------------------------------------------------------------

        ----------------------------------------------------------------------------------------------------------------------------------------------------------------------

        IF OBJECT_ID('dbo.WEKA_INQUEUE_STATS', 'U') IS NULL
        CREATE TABLE WEKA_INQUEUE_STATS (INQUEUEID BIGINT, ACTIONQUEUEID BIGINT, SENT_DT DATETIME, f_selligent_campaign INT, f_selligent_mail INT, f_selligent_email_state INT,
        NB_OPEN INT, NB_CLIC INT, LAST_CLIC_DT DATETIME, LAST_OPEN_DT DATETIME,f_profile_id NVARCHAR(100), USERID INT, LISTID INT, PREVIOUS_HASH NVARCHAR(32), CURRENT_HASH  NVARCHAR(32), CONSTRAINT IDX_WEKA_INQUEUE UNIQUE(INQUEUEID));

        ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
        ----------------------------------------------------------------------------------------------------------------------------------------------------------------------

        DELETE FROM  WEKA_INQUEUE_STATS
        WHERE SENT_DT <  GETDATE()-15;

        UPDATE WEKA_INQUEUE_STATS
        SET PREVIOUS_HASH = CURRENT_HASH;


    IF DATEPART(HOUR,GETDATE()) >= 8 AND DATEPART(HOUR,GETDATE()) <= 18

            ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
            -- Insert les données de la table actionqueue dans la WEKA_INQUEUE_STATS des 15 derniers minutes
            ----------------------------------------------------------------------------------------------------------------------------------------------------------------------

            MERGE INTO WEKA_INQUEUE_STATS T
                USING (
                    SELECT
                    T1.INQUEUEID, T1.ID, T1.SENT_DT, T1.CAMPAIGNID, T1.MAILID, T1.STATE, T2.PROFILE_ID, T1.USERID, T1.LISTID
                    FROM actionqueue T1 with (nolock)
                    INNER JOIN USERS_WEKA_EMAILS T2  with (nolock) ON T1.USERID = T2.ID 
                    WHERE T1.SENT_DT  >=  DATEADD(MI,-20,GETDATE()) AND T1.LISTID = 284
                    ) S
            ON (T.INQUEUEID = S.INQUEUEID)
            WHEN MATCHED AND T.f_profile_id is null 
            THEN
                UPDATE
                SET f_profile_id = S.PROFILE_ID

            WHEN NOT MATCHED 
            THEN
                INSERT (INQUEUEID, ACTIONQUEUEID, SENT_DT, f_selligent_campaign, f_selligent_mail, f_selligent_email_state, f_profile_id,USERID,LISTID)
                VALUES (S.INQUEUEID, S.ID, S.SENT_DT, S.CAMPAIGNID, S.MAILID, S.STATE, S.PROFILE_ID, S.USERID, S.LISTID);

            ------------------------------------------------------------------------------------------------------------------

    ELSE 
    BEGIN

            ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
            -- Insert les données de la table actionqueue dans la WEKA_INQUEUE_STATS des 15 derniers jours
            ----------------------------------------------------------------------------------------------------------------------------------------------------------------------

            MERGE INTO WEKA_INQUEUE_STATS T
                USING (
                    SELECT
                    T1.INQUEUEID, T1.ID, T1.SENT_DT, T1.CAMPAIGNID, T1.MAILID, T1.STATE, T2.PROFILE_ID, T1.USERID, T1.LISTID
                    FROM actionqueue T1 with (nolock)
                    INNER JOIN USERS_WEKA_EMAILS T2  with (nolock) ON T1.USERID = T2.ID 
                    WHERE T1.SENT_DT  >= GETDATE()-15 AND T1.LISTID = 284
                    ) S
            ON (T.INQUEUEID = S.INQUEUEID)
            WHEN MATCHED AND T.f_profile_id is null 
            THEN
                UPDATE
                SET f_profile_id = S.PROFILE_ID

            WHEN NOT MATCHED 
            THEN
                INSERT (INQUEUEID, ACTIONQUEUEID, SENT_DT, f_selligent_campaign, f_selligent_mail, f_selligent_email_state, f_profile_id,USERID,LISTID)
                VALUES (S.INQUEUEID, S.ID, S.SENT_DT, S.CAMPAIGNID, S.MAILID, S.STATE, S.PROFILE_ID, S.USERID, S.LISTID);

            ------------------------------------------------------------------------------------------------------------------

            MERGE INTO WEKA_INQUEUE_STATS T
                USING (
                    SELECT
                    T1.INQUEUEID,
                    SUM(CASE WHEN T2.PROBEID= -1 THEN 1  ELSE 0 END) AS NB_OPEN,
                    SUM(CASE WHEN T2.PROBEID>200 THEN 1  ELSE 0 END) AS NB_CLIC,
                    MAX(CASE WHEN T2.PROBEID= -1 THEN DT  ELSE NULL END) LAST_OPEN_DT,
                    MAX(CASE WHEN T2.PROBEID>200 THEN DT  ELSE NULL END) LAST_CLIC_DT
                    FROM WEKA_INQUEUE_STATS T1
                    INNER JOIN FLAGS T2 with (nolock) ON T1.INQUEUEID=T2.INQUEUEID AND T1.USERID = T2.USERID AND T1.LISTID = T2.LISTID AND T1.f_selligent_campaign = T2.CAMPAIGNID AND T2.DT >= GETDATE()-15
                    GROUP BY T1.INQUEUEID
                    ) S
            ON (T.INQUEUEID = S.INQUEUEID)
            WHEN MATCHED THEN 

                UPDATE 
                SET 
                T.NB_OPEN = S.NB_OPEN,
                T.NB_CLIC = S.NB_CLIC,
                T.LAST_OPEN_DT = S.LAST_OPEN_DT,
                T.LAST_CLIC_DT = S.LAST_CLIC_DT
            ;
    END
    ------------------------------------------------------------------------------------------------------------------
    -- Crétion d'un Hash sur l'ensemble des données afin de verifier quelle ligne à été modifier 
    ------------------------------------------------------------------------------------------------------------------
    UPDATE T1 
    SET CURRENT_HASH = CONVERT(NVARCHAR(32),HASHBYTES('MD5', CONCAT(T1.INQUEUEID,T1.ACTIONQUEUEID,T1.NB_OPEN,T1.NB_CLIC,T1.LAST_OPEN_DT,T1.LAST_CLIC_DT,f_profile_id)),2) 
    FROM WEKA_INQUEUE_STATS T1;

    ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ----------------------------------------------------------------------------------------------------------------------------------------------------------------------

    SELECT
    ACTIONQUEUEID,
        FORMAT(SENT_DT,'yyyy-MM-dd HH:mm:ss') as SENT_DT,
    f_selligent_campaign, 
    f_selligent_mail,
    f_selligent_email_state,
    NB_OPEN,
    NB_CLIC,
        FORMAT(LAST_OPEN_DT,'yyyy-MM-dd HH:mm:ss') as LAST_OPEN_DT,
        FORMAT(LAST_CLIC_DT,'yyyy-MM-dd HH:mm:ss') as LAST_CLIC_DT,
    f_profile_id
    FROM WEKA_INQUEUE_STATS T1 with (nolock)
    WHERE (T1.CURRENT_HASH <> T1.PREVIOUS_HASH OR  T1.PREVIOUS_HASH is null) AND f_profile_id is not null;

END