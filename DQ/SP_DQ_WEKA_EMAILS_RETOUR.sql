AS
BEGIN
SET NOCOUNT ON;

-- à 4h on met à jour toute la base 
IF(datepart(hour, getdate())=4)
BEGIN
	update T0
	set MAIL_RETOUR = case 
		WHEN MAX_ABO.ACTIVE_TO >= cast(getdate() as date) THEN 'support.fidelisation@weka.fr'
		else 'bounces@weka.fr'
	end
	FROM USERS_WEKA_EMAILS T0
	inner join (Select T1.ID, max(ACTIVE_TO) ACTIVE_TO FROM USERS_WEKA_EMAILS T1
	left join DATA_WEKA_CIAM_PROFILE_SUB T2 on T1.PROFILE_ID = T2.F_PROFILE_ID
	left join DATA_WEKA_CIAM_ACCNT_SUB T3 on T2.F_ACCOUNT_SUBSCRIPTION_ID=T3.ACCOUNT_SUBSCRIPTION_ID 
	GROUP BY T1.ID) MAX_ABO
	on T0.ID = MAX_ABO.ID
END

-- on ne met à jour que les emails n'ayant pas de MAIL_RETOUR défined
IF(datepart(hour, getdate())<>4)
BEGIN
	update T0
	set MAIL_RETOUR = case 
		WHEN MAX_ABO.ACTIVE_TO >= cast(getdate() as date) THEN 'support.fidelisation@weka.fr'
		else 'bounces@weka.fr'
	end
	FROM USERS_WEKA_EMAILS T0
	inner join (Select T1.ID, max(ACTIVE_TO) ACTIVE_TO FROM USERS_WEKA_EMAILS T1
	left join DATA_WEKA_CIAM_PROFILE_SUB T2 on T1.PROFILE_ID = T2.F_PROFILE_ID
	left join DATA_WEKA_CIAM_ACCNT_SUB T3 on T2.F_ACCOUNT_SUBSCRIPTION_ID=T3.ACCOUNT_SUBSCRIPTION_ID 
	GROUP BY T1.ID) MAX_ABO
	on T0.ID = MAX_ABO.ID
	WHERE MAIL_RETOUR IS NULL
END


END