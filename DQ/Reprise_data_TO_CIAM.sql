--ticket : https://redmine.weka-ssc.fr/issues/10196
-- REPRISE des données des contacts pour enrichir le CIAM 
AS
BEGIN
SET NOCOUNT ON;
select 
T1.MAIL
,T2.MODIFIED_DT
,T7.CODE AS SERVICE
,T8.CODE as FONCTION
,T9.CODE as activity
,T10.CODE as account_type
,T5.RS1
,T6.ADDRESS_POSTALCODE
,T6.ADDRESS_CITY
from USERS_WEKA_EMAILS T1 with (nolock)
inner join USERS_TISSOT_CONTACT T2 with (nolock) on T1.MAIL_CODE = T2.MAIL_CODE
left join DATA_WEKA_PP_INFOSPEC T3 with (nolock) on T2.ID = T3.CONTACT_ID
left join DATA_WEKA_PM_INFOSPEC T4 with (nolock) on T2.COMPTE_ID = T4.COMPTE_ID
left join USERS_WEKA_COMPTE T5 with (nolock) on T5.ID = T2.COMPTE_ID
left join DATA_WEKA_PM_ADRPRI T6 with (nolock) on T5.ID = T6.COMPTE_ID
left join DATA_PP_DEPARTMENT T7 with (nolock) on T3.DEPARTMENT_CODE = T7.ID
left join DATA_PP_TITLE T8 with (nolock) on T3.TITLE_CODE = T8.ID
left join DATA_PM_ACTIVITY T9 with (nolock) on T4.ACTIVITY_CODE = T9.ID
left join DATA_PM_ACCOUNT_TYPE T10 with (nolock) on T4.ACCOUNT_TYPE_CODE = T10.ID
where len(CONCAT(T7.CODE
,T8.CODE
,T9.CODE
,T10.CODE
,T5.RS1
,T6.ADDRESS_POSTALCODE
,T6.ADDRESS_CITY))>0
and T1.OPTOUT is null
END