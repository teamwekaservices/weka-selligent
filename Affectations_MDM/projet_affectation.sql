select gwcp.postal_code, gwcp.f_secteur_activite, gwcl.f_type_leads,
gwacd.f_wk_aff_com_dep ,
gwact.f_wk_affectations_commercia ,
gwacs.f_wk_affectations_commercia,
COALESCE (gwacd.f_wk_aff_com_dep, gwact.f_wk_affectations_commercia,gwacs.f_wk_affectations_commercia)  as affectation
from gd_wk_ciam_lead gwcl 
inner join gd_wk_ciam_profile gwcp on gwcl.f_profile_id = gwcp.profile_id 
left join gd_wk_aff_com_dep gwacd on gwacd.f_departements_fr =  left(gwcp.postal_code,2)
left join gd_wk_aff_comm_type gwact on gwact.f_type_leads = gwcl.f_type_leads 
left join gd_wk_aff_com_secteur gwacs on gwacs.f_secteur_activite = gwcp.f_secteur_activite
where gwcl.lead_id = '1987e348-f2c5-11ec-9cac-0242a97a9e4c'
and (case 
	when gwacs.f_wk_affectations_commercia is null and gwacd.f_wk_aff_com_dep = gwact.f_wk_affectations_commercia then 1
end)=1


SELECT max(x) FROM unnest(c) as x;



select * from gd_WK_AFF_COM_SECTEUR


-- parametre à passer : 
--- code postal : gwcp.postal_code
--- secteur activité : gwcp.f_secteur_activite
--- type de leads : gwcl.f_type_leads
-- sortie : 
--- id du commercial 


--- 


CREATE OR REPLACE FUNCTION weka_mdm.wk_ciam_affectation_leads(p_postal_code text, p_secteur_activite text, p_profile_id text)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
BEGIN
    DECLARE
        v_affectation text;
    BEGIN
       v_affectation = '';
	    execute 'SELECT profile_id FROM weka_mdm.'||p_table||' WHERE email::text = '||quote_literal(p_email)||';' INTO v_isRCU_tmp;
    
	   	if v_isRCU_tmp is null 
	   		then 
	   			RETURN (v_isRCU) ;
	   		else 
	   			return (v_isRCU_tmp);
	   end if ;
	   
    END;
END;
$function$
;
