AS
BEGIN
SET NOCOUNT ON;


/*Liste des critères 


OK : date de dernier clic (hors clic sur lien de désabo sur nos e-mailings, infocoll/alertes MAJ et e-newsletters)
OK : date de dernière facture(one shot, formation ou réabo)
OK : Date de fin d’abonnement
OK : date de dernier téléchargement/DI/Formulaire sur le site (tous formulaires dontwebcallback + inscription webinar et replay + abandon de panier)
OK date de dernier RDV effectué tél ou terrain
NON PAS TI date de dernière participation à une session de formation
OK date de dernier abonnement à une newsletter (peut être différent du formulaire notamment chez TI avec enregistrement sur un formulaire partenaire)
NON PAS TI date de dernière authentification sur le site internet (login, pas cookie)
 date intégration fichier
OK Date de dernière création de devis (dans Selligent)
OK Appels assimilés à une activité (cf fichier « Statuts appels retenus – Toutes sociétés »
(1)


*/


----------------------------------------------------------------------------------------------------
------------------- 		DEBUT SP_DQ_WEKA_ACTIVITY_DT_01 		  ------------------------------
----------------------------------------------------------------------------------------------------


--- 
--- DATE DERNIER CLIC
---
-- check temp table
IF OBJECT_ID('dbo.TMP_WEKA_ACTIVITE_EMAIL', 'U') IS NOT NULL
BEGIN;
	DROP TABLE dbo.TMP_WEKA_ACTIVITE_EMAIL;
END;

-- création table temporaire pour stocker les clics des emails
CREATE TABLE dbo.TMP_WEKA_ACTIVITE_EMAIL (
	MAIL_CODE INT NOT NULL,
	CLICK_LASTDT DATE
);


-- on vide la table des activités 
--TRUNCATE TABLE dbo.TMP_WEKA_ACTIVITE_EMAIL;
--TRUNCATE TABLE DATA_WEKA_DATE_ACTIVITE;

-- insertion des dates de clics pour les contacts
INSERT INTO dbo.TMP_WEKA_ACTIVITE_EMAIL (CLICK_LASTDT,MAIL_CODE)
SELECT CLICK_LASTDT,MAIL_CODE FROM TAXONOMY_L327_T6 T1 WITH (NOLOCK)
INNER JOIN USERS_WEKA_CONTACT T2 WITH (NOLOCK) ON T1.USERID=T2.ID AND T2.MAIL_CODE IS NOT NULL
WHERE CLICK_LASTDT IS NOT NULL; -- pour init ne pas prendre le critère de date

-- insertion des dates de clics pour les suspects
INSERT INTO dbo.TMP_WEKA_ACTIVITE_EMAIL (CLICK_LASTDT,MAIL_CODE)
SELECT CLICK_LASTDT,MAIL_CODE FROM TAXONOMY_L328_T6 T1 WITH (NOLOCK)
INNER JOIN USERS_WEKA_SUSPECTS T2 WITH (NOLOCK) ON T1.USERID=T2.ID AND T2.MAIL_CODE IS NOT NULL
WHERE CLICK_LASTDT IS NOT NULL; -- pour init ne pas prendre le critère de date

-- insertion des dates de clics pour les emails (users)
INSERT INTO dbo.TMP_WEKA_ACTIVITE_EMAIL (CLICK_LASTDT,MAIL_CODE)
SELECT CLICK_LASTDT,USERID FROM TAXONOMY_L284_T6 T1 WITH (NOLOCK)
WHERE CLICK_LASTDT IS NOT NULL; -- pour init ne pas prendre le critère de date

-- insertion des dates de clics pour les HORS ANTENNE
INSERT INTO dbo.TMP_WEKA_ACTIVITE_EMAIL (CLICK_LASTDT,MAIL_CODE)
SELECT CLICK_LASTDT,MAIL_CODE FROM TAXONOMY_L331_T6 T1 WITH (NOLOCK)
INNER JOIN USERS_WEKA_HORS_ANTENNE T2 WITH (NOLOCK) ON T1.USERID=T2.ID AND T2.MAIL_CODE IS NOT NULL
WHERE CLICK_LASTDT IS NOT NULL; -- pour init ne pas prendre le critère de date

-- creation d'index
CREATE INDEX IDX_MASTER_CODE ON dbo.TMP_WEKA_ACTIVITE_EMAIL (MAIL_CODE);
CREATE INDEX IDX_MASTER_CODE2 ON dbo.TMP_WEKA_ACTIVITE_EMAIL (MAIL_CODE,CLICK_LASTDT);


-- Merge dans le table DATA_WEKA_EMAILS de la MAX CLICK_LASTDT 
MERGE USERS_WEKA_EMAILS AS T
USING (SELECT MAX(CLICK_LASTDT) AS CLICK_LASTDT,MAIL_CODE FROM dbo.TMP_WEKA_ACTIVITE_EMAIL GROUP BY MAIL_CODE)  AS S
ON (T.ID = S.MAIL_CODE) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.CLICK_LAST_DT=S.CLICK_LASTDT;
			
-- 18s 
-- vidage de la table DATA_WEKA_DATE_ACTIVITE et purge des index 
TRUNCATE TABLE DATA_WEKA_DATE_ACTIVITE;
DROP INDEX IF EXISTS
    IDX_LISTID_USERID_DATE_ACTIVITE ON DATA_WEKA_DATE_ACTIVITE,
    IDX_WEKA_DATE_ACTIVITE_LISTID ON DATA_WEKA_DATE_ACTIVITE,
    IDX_WEKA_DATE_ACTIVITE_LISTID_DATE ON DATA_WEKA_DATE_ACTIVITE,
    IDX_WEKA_DATE_ACTIVITE_LISTID_USER ON DATA_WEKA_DATE_ACTIVITE,
    IDX_WEKA_DATE_ACTIVITE_LISTID ON DATA_WEKA_DATE_ACTIVITE,
    IDX_WEKA_DATE_ACTIVITE_MAIL_CODE ON DATA_WEKA_DATE_ACTIVITE,
    IDX_TMP_ACT_LISTID ON DATA_WEKA_DATE_ACTIVITE,
    IDX_USERID_DATE_ACTIVITE ON DATA_WEKA_DATE_ACTIVITE;

			
-- insertion dans la table de date d'activité de la dernière date de clic par contact 
INSERT INTO DATA_WEKA_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT  
	284,
	T1.ID,
	1,
	MAX(CLICK_LAST_DT),
	T1.MAIL_CODE
FROM USERS_WEKA_CONTACT T1 
INNER JOIN USERS_WEKA_EMAILS T2 ON T2.ID = T1.MAIL_CODE AND CLICK_LAST_DT IS NOT NULL
GROUP BY T1.ID,T1.MAIL_CODE;

-- insertion dans la table de date d'activité de la dernière date de clic par suspect
INSERT INTO DATA_WEKA_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT  
	328,
	T1.ID,
	1,
	MAX(CLICK_LAST_DT),
	T1.MAIL_CODE
FROM USERS_WEKA_SUSPECTS T1 
INNER JOIN USERS_WEKA_EMAILS T2 ON T2.ID = T1.MAIL_CODE AND CLICK_LAST_DT IS NOT NULL
GROUP BY T1.ID,T1.MAIL_CODE;

-- insertion dans la table de date d'activité de la dernière date de clic par emails
INSERT INTO DATA_WEKA_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT  
	284,
	T1.ID,
	1,
	CLICK_LAST_DT,
	T1.MAIL_CODE
FROM USERS_WEKA_EMAILS T1
WHERE CLICK_LAST_DT IS NOT NULL;

-- insertion dans la table de date d'activité de la dernière date de clic par hors_antenne
INSERT INTO DATA_WEKA_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT  
	331,
	T1.ID,
	1,
	MAX(CLICK_LAST_DT),
	T1.MAIL_CODE
FROM USERS_WEKA_HORS_ANTENNE T1 
INNER JOIN USERS_WEKA_EMAILS T2 ON T2.ID = T1.MAIL_CODE AND CLICK_LAST_DT IS NOT NULL
GROUP BY T1.ID,T1.MAIL_CODE;


--------------------------------------------------------------------------------------------------------------------------------------------
--- 
--- DATE DERNIERE STATS WEB WEKA 
--- 
-- Aggragations des stats envoyés par le site web WEKA 
INSERT INTO DATA_WEKA_DATE_ACTIVITE (LISTID,TYPE_ACTIVITE,EMAIL_ID,DATE_ACTIVITE)
SELECT 284, 12,MAIL_CODE, (SELECT Max(v) FROM (VALUES (LAST_CONNEXION_DT),(CREATION_COMPTE_DT),(LAST_DL_OUTIL_DT),(LAST_CONSULTATION_OUTIL_DT),(LAST_DL_FICHE_DT),(WLE_CREATED_DT)) AS value(v)) AS DATE_MAX
FROM DATA_WEKA_STATS_WEB

-- idem pour les leads du CIAM 


--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DERNIER ABONNEMENT
--- uniquement pour les contacts, car pas de ABONNEMENTS pour les suspects
--- 

-- sélection des contacts dont le compte a un abonnement
INSERT INTO DATA_WEKA_DATE_ACTIVITE (LISTID,TYPE_ACTIVITE,EMAIL_ID,DATE_ACTIVITE)
SELECT
	284,
	3,
	T1.ID,
	MAX(FIN_ABO_DT) 
FROM DATA_WEKA_ABOS_SAP T1
GROUP BY T1.ID;


-- 11s


--------------------------------------------------------------------------------------------------------------------------------------------
--- 
--- DATE DERNIERE ACTIVITE WEB
--- 
-- sélection des contacts ayant une activité web 
INSERT INTO DATA_WEKA_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT
	284,
	T1.MAIL_CODE,
	4,
	MAX(ACTIVITE_DT) 
	,MAIL_CODE
FROM DATA_WEKA_WEB_ACTIVITIES T1 
GROUP BY T1.MAIL_CODE;

--------------------------------------------------------------------------------------------------------------------------------------------
--- 
--- DATE DERNIER ENVOI EMT
--- 
-- sélection des contacts ayant une activité web 
INSERT INTO DATA_WEKA_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT
	284,
	T1.USERID,
	13,
	MAX(CREATED_DT) 
	,T1.USERID
FROM ACTION_WEKA_EMT T1 
GROUP BY T1.USERID;


----------------------------------------------------------------------------------------------------
------------------- 		FIN SP_DQ_WEKA_ACTIVITY_DT_01 		  ------------------------------
----------------------------------------------------------------------------------------------------





----------------------------------------------------------------------------------------------------
------------------- 		DEBUT SP_DQ_WEKA_ACTIVITY_DT_02 		  ------------------------------
----------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DERNIERE ABONNEMENT NEWSLETTER 
--- 
-- sélection des contacts ayant une inscription à une newsletter 
INSERT INTO DATA_WEKA_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT
	284,
	T1.MAIL_CODE,
	5,
	MAX(DATE_INSCRIPTION) ,
	T1.MAIL_CODE
FROM DATA_WEKA_NL T1 
WHERE DATE_INSCRIPTION IS NOT NULL
GROUP BY T1.MAIL_CODE;


--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DEVIS AFFAIRE ATHENA 
--- 
-- sélection des contacts ayant une ligne de commande (affaire dans Athéna) 
INSERT INTO DATA_WEKA_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT 
	327,
	CONTACT_ID,
	7,
	MAX(DATE_COMMANDE),
	T2.MAIL_CODE 
FROM Data_WEKA_AFFAIRES T1
INNER JOIN USERS_WEKA_CONTACT T2 with (nolock) ON T2.ID = T1.CONTACT_ID AND CONTACT_ID IS NOT NULL
GROUP BY CONTACT_ID,T2.MAIL_CODE;


-- 39s

--------------------------------------------------------------------------------------------------------------------------------------------


--- DATE CREATION 
--- 
-- sélection des contacts  
INSERT INTO DATA_WEKA_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT
	327,
	USERS_WEKA_CONTACT.ID,
	11,
	CREATED_DT,
	MAIL_CODE
FROM USERS_WEKA_CONTACT WITH (NOLOCK)
WHERE CREATED_DT IS NOT NULL ;

-- sélection des suspects
INSERT INTO DATA_WEKA_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT
	328,
	USERS_WEKA_SUSPECTS.ID,
	11,
	DATE_ACTIVATION_COMPTE,
	MAIL_CODE
FROM USERS_WEKA_SUSPECTS 
WHERE DATE_ACTIVATION_COMPTE IS NOT NULL  ;


-- sélection des emails 
INSERT INTO DATA_WEKA_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT
	284,
	T1.ID,
	11,
	CREATED_DT,
	T1.ID 
FROM USERS_WEKA_EMAILS T1 
WHERE CREATED_DT IS NOT NULL ;

-- sélection des hors antenne 
INSERT INTO DATA_WEKA_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT
	331,
	T1.ID,
	11,
	CREATED_DT,
	T1.MAIL_CODE 
FROM USERS_WEKA_HORS_ANTENNE T1 
WHERE CREATED_DT IS NOT NULL ;


--41s
--------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------
------------------- 		FIN SP_DQ_WEKA_ACTIVITY_DT_02 		  ------------------------------
----------------------------------------------------------------------------------------------------





----------------------------------------------------------------------------------------------------
------------------- 	DEBUT SP_DQ_WEKA_ACTIVITY_DT_MERGE_01		  ------------------------------
----------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
-- MISE A JOUR des MAIL_CODE pour toutes les tables  


MERGE DATA_WEKA_DATE_ACTIVITE AS T
USING (
	SELECT T1.ID, T2.MAIL_CODE
	FROM DATA_WEKA_DATE_ACTIVITE T1 
	INNER JOIN USERS_WEKA_CONTACT T2 ON T1.USERID=T2.ID
	WHERE LISTID=327 AND T1.EMAIL_ID IS NULL AND T2.MAIL_CODE IS NOT NULL
)  AS S
ON (T.ID = S.ID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.EMAIL_ID=S.MAIL_CODE;


MERGE DATA_WEKA_DATE_ACTIVITE AS T
USING (
	SELECT T1.ID, T2.MAIL_CODE
	FROM DATA_WEKA_DATE_ACTIVITE T1 
	INNER JOIN USERS_WEKA_SUSPECTS T2 ON T1.USERID=T2.ID
	WHERE LISTID=328 AND T1.EMAIL_ID IS NULL
)  AS S
ON (T.ID = S.ID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.EMAIL_ID=S.MAIL_CODE;




-- INSERTION des dates dans les utilisateurs 

-- selection et insertion de la dernière date d'activité par contact
MERGE USERS_WEKA_CONTACT AS T
USING (SELECT MAX(DATE_ACTIVITE) AS DATE_ACTIVITE, USERID
FROM DATA_WEKA_DATE_ACTIVITE
WHERE LISTID=327
GROUP BY USERID)  AS S
ON (T.ID = S.USERID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DATE_ACTIVITE;

-- selection et insertion de la dernière date d'activité par suspect
MERGE USERS_WEKA_SUSPECTS AS T
USING (SELECT MAX(DATE_ACTIVITE) AS DATE_ACTIVITE, USERID
FROM DATA_WEKA_DATE_ACTIVITE
WHERE LISTID=328
GROUP BY USERID)  AS S
ON (T.ID = S.USERID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DATE_ACTIVITE;
			
			
-- selection et insertion de la dernière date d'activité par emails
MERGE USERS_WEKA_EMAILS AS T
USING (SELECT MAX(DATE_ACTIVITE) AS DATE_ACTIVITE, EMAIL_ID AS USERID
FROM DATA_WEKA_DATE_ACTIVITE
WHERE EMAIL_ID IS NOT NULL
GROUP BY EMAIL_ID)  AS S
ON (T.MAIL_CODE = S.USERID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DATE_ACTIVITE;




-- selection et insertion de la dernière date d'activité par hors antenne
MERGE USERS_WEKA_HORS_ANTENNE AS T
USING (SELECT MAX(DATE_ACTIVITE) AS DATE_ACTIVITE, USERID
FROM DATA_WEKA_DATE_ACTIVITE
WHERE LISTID=331
GROUP BY USERID)  AS S
ON (T.ID = S.USERID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DATE_ACTIVITE;



--- alignement des dates d'activités
MERGE USERS_WEKA_EMAILS AS T
USING (
	SELECT USERS_WEKA_EMAILS.ID,MAX(USERS_WEKA_CONTACT.DERNIERE_ACTIVITE_DT) AS DERNIERE_ACTIVITE_DT
 	FROM USERS_WEKA_EMAILS
	INNER JOIN USERS_WEKA_CONTACT ON USERS_WEKA_CONTACT.MAIL_CODE = USERS_WEKA_EMAILS.MAIL_CODE AND USERS_WEKA_EMAILS.DERNIERE_ACTIVITE_DT < USERS_WEKA_CONTACT.DERNIERE_ACTIVITE_DT
GROUP BY USERS_WEKA_EMAILS.ID
)  AS S
ON (T.ID = S.ID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DERNIERE_ACTIVITE_DT;


MERGE USERS_WEKA_EMAILS AS T
USING (SELECT USERS_WEKA_EMAILS.ID,MAX(USERS_WEKA_SUSPECTS.DERNIERE_ACTIVITE_DT) AS DERNIERE_ACTIVITE_DT
 FROM USERS_WEKA_EMAILS
INNER JOIN USERS_WEKA_SUSPECTS ON USERS_WEKA_SUSPECTS.MAIL_CODE = USERS_WEKA_EMAILS.MAIL_CODE AND USERS_WEKA_EMAILS.DERNIERE_ACTIVITE_DT < USERS_WEKA_SUSPECTS.DERNIERE_ACTIVITE_DT
GROUP BY USERS_WEKA_EMAILS.ID
)  AS S
ON (T.ID = S.ID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DERNIERE_ACTIVITE_DT;
			
MERGE USERS_WEKA_EMAILS AS T
USING (SELECT USERS_WEKA_EMAILS.ID,MAX(USERS_WEKA_HORS_ANTENNE.DERNIERE_ACTIVITE_DT) AS DERNIERE_ACTIVITE_DT
 FROM USERS_WEKA_EMAILS
INNER JOIN USERS_WEKA_HORS_ANTENNE ON USERS_WEKA_HORS_ANTENNE.MAIL_CODE = USERS_WEKA_EMAILS.MAIL_CODE AND USERS_WEKA_EMAILS.DERNIERE_ACTIVITE_DT < USERS_WEKA_HORS_ANTENNE.DERNIERE_ACTIVITE_DT
GROUP BY USERS_WEKA_EMAILS.ID
)  AS S
ON (T.ID = S.ID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DERNIERE_ACTIVITE_DT;






MERGE USERS_WEKA_CONTACT AS T
USING (
	SELECT USERS_WEKA_CONTACT.ID,USERS_WEKA_EMAILS.DERNIERE_ACTIVITE_DT
 	FROM USERS_WEKA_CONTACT
	INNER JOIN USERS_WEKA_EMAILS ON USERS_WEKA_CONTACT.MAIL_CODE = USERS_WEKA_EMAILS.MAIL_CODE AND USERS_WEKA_EMAILS.DERNIERE_ACTIVITE_DT> USERS_WEKA_CONTACT.DERNIERE_ACTIVITE_DT
)  AS S
ON (T.ID = S.ID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DERNIERE_ACTIVITE_DT;



MERGE USERS_WEKA_SUSPECTS AS T
USING (SELECT USERS_WEKA_SUSPECTS.ID,USERS_WEKA_EMAILS.DERNIERE_ACTIVITE_DT
 FROM USERS_WEKA_SUSPECTS
INNER JOIN USERS_WEKA_EMAILS ON USERS_WEKA_SUSPECTS.MAIL_CODE = USERS_WEKA_EMAILS.MAIL_CODE AND USERS_WEKA_EMAILS.DERNIERE_ACTIVITE_DT> USERS_WEKA_SUSPECTS.DERNIERE_ACTIVITE_DT
)  AS S
ON (T.ID = S.ID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DERNIERE_ACTIVITE_DT;


MERGE USERS_WEKA_HORS_ANTENNE AS T
USING (SELECT USERS_WEKA_HORS_ANTENNE.ID,USERS_WEKA_EMAILS.DERNIERE_ACTIVITE_DT
 FROM USERS_WEKA_HORS_ANTENNE
INNER JOIN USERS_WEKA_EMAILS ON USERS_WEKA_HORS_ANTENNE.MAIL_CODE = USERS_WEKA_EMAILS.MAIL_CODE AND USERS_WEKA_EMAILS.DERNIERE_ACTIVITE_DT> USERS_WEKA_HORS_ANTENNE.DERNIERE_ACTIVITE_DT
)  AS S
ON (T.ID = S.ID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DERNIERE_ACTIVITE_DT;






-- toppage des contacts ayant 3 ans dépassés
UPDATE USERS_WEKA_CONTACT
SET
	OPTOUT=999,
	OPTOUT_SOURCE='PURGE RGPD',
	OPTOUT_DT=GETDATE()
WHERE (DERNIERE_ACTIVITE_DT < DATEADD(year,-3,GETDATE()) OR DERNIERE_ACTIVITE_DT IS NULL) AND (OPTOUT <> 999 OR OPTOUT IS NULL) ;


UPDATE USERS_WEKA_SUSPECTS
SET
	OPTOUT=999,
	OPTOUT_SOURCE='PURGE RGPD',
	OPTOUT_DT=GETDATE()
WHERE (DERNIERE_ACTIVITE_DT < DATEADD(year,-3,GETDATE()) OR DERNIERE_ACTIVITE_DT IS NULL) AND (OPTOUT <> 999 OR OPTOUT IS NULL) ;

UPDATE USERS_WEKA_EMAILS
SET
	OPTOUT=999,
	OPTOUT_SOURCE='PURGE RGPD',
	OPTOUT_DT=GETDATE()
WHERE (DERNIERE_ACTIVITE_DT < DATEADD(year,-3,GETDATE()) OR DERNIERE_ACTIVITE_DT IS NULL) AND (OPTOUT <> 999 OR OPTOUT IS NULL) ;


-- réabilitation des personnes ayant un comportement alors qu'ils étaient en 999

UPDATE USERS_WEKA_CONTACT
SET
	OPTOUT=NULL,
	OPTOUT_SOURCE=NULL,
	OPTOUT_DT=NULL
WHERE DERNIERE_ACTIVITE_DT > DATEADD(year,-3,GETDATE()) AND OPTOUT = 999;

UPDATE USERS_WEKA_EMAILS
SET
	OPTOUT=NULL,
	OPTOUT_SOURCE=NULL,
	OPTOUT_DT=NULL
WHERE DERNIERE_ACTIVITE_DT > DATEADD(year,-3,GETDATE()) AND OPTOUT = 999;


UPDATE USERS_WEKA_SUSPECTS
SET
	OPTOUT=NULL,
	OPTOUT_SOURCE=NULL,
	OPTOUT_DT=NULL
WHERE DERNIERE_ACTIVITE_DT > DATEADD(year,-3,GETDATE()) AND OPTOUT = 999;

end