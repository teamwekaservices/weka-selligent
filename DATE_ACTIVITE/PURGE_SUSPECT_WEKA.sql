
ASBEGINSET NOCOUNT ON;-- création de la table temporaire pour stocker les ID de la table USERS_WEKA_SUSPECT qui sont à purger-- création de la table temporaire pour stocker les ID de la table USERS_WEKA_SUSPECT qui sont à purger
IF OBJECT_ID('dbo.TMP_WEKA_SUSPECT_PURGE', 'U') IS NULL
BEGIN;
	CREATE TABLE dbo.TMP_WEKA_SUSPECT_PURGE (
		USERID INT NOT NULL,
		PURGE_DT DATETIME,
		ID_SUSPECT VARCHAR(255)	);
END;
-- remplissage des contacts à purger -- /!\ requête temporaire à adapterINSERT INTO dbo.TMP_WEKA_SUSPECT_PURGE (USERID,PURGE_DT,ID_SUSPECT)SELECT ID,GETDATE(),MAIL FROM USERS_WEKA_SUSPECTS WHERE OPTOUT=999 AND (DERNIERE_ACTIVITE_DT < DATEADD(yy,-3,GETDATE()) OR DERNIERE_ACTIVITE_DT IS NULL) ; ------------------------------------------------------------------ SUPPRESSION DES DONNEES LIEES AU CONTACT ---------- -------------------------------------------------------------Suppression des taxonmieDELETE T1 FROM TAXONOMY_L328_T6 AS T1WHERE EXISTS (SELECT 1 FROM TMP_WEKA_SUSPECT_PURGE TP WHERE TP.USERID = T1.USERID);  -- SUPPRESSION DU SUSPECT DELETE T1 FROM USERS_WEKA_SUSPECTS AS T1WHERE EXISTS (SELECT 1 FROM TMP_WEKA_SUSPECT_PURGE TP WHERE TP.USERID = T1.ID AND PURGE_DT > getdate()-1 ) ;     END