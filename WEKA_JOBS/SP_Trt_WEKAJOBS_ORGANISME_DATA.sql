MERGE DATA_WKJOBS_ORGANISMES AS T 
USING (SELECT  
	ID_ORGANISME
	,NOM_ORGANISME
	,ORGANISME_VALIDE
	,CODE_POSTAL
	,VILLE
	,REGION
	,ID_TYPE_ORGANISME
	,PACK_VISIBILITE
	,PACK_ENTREPRISE_V2
	,PAGE_EMPLOYEUR_V2_PUBLIEE
	,NBR_JETONS_ACHETES
	,DATE_EXPIRATION_JETONS
	,NBR_JETONS_RESTANT
	,NBR_ANNONCES_TOTALES
	,NBR_ANNONCES_ACTIVES
	,NBR_ANNONCES_EN_LIGNE_MOIS
	,NBR_ANNONCES_EN_LIGNE_MOIS_PASSE
FROM TMP_WKJOBS_ORGANISMES_DATA WHERE SIM_PARSELINE_RESULT=1) AS S
ON (S.ID_ORGANISME =T.ID_ORGANISME)
WHEN NOT MATCHED BY TARGET
	THEN 
		INSERT (ID_ORGANISME
		,NOM_ORGANISME
		,ORGANISME_VALIDE
		,CODE_POSTAL
		,VILLE
		,REGION
		,ID_TYPE_ORGANISME
		,PACK_VISIBILITE
		,PACK_ENTREPRISE_V2
		,PAGE_EMPLOYEUR_V2_PUBLIEE
		,NBR_JETONS_ACHETES
		,DATE_EXPIRATION_JETONS
		,NBR_JETONS_RESTANT
		,NBR_ANNONCES_TOTALES
		,NBR_ANNONCES_ACTIVES
		,NBR_ANNONCES_EN_LIGNE_MOIS
		,NBR_ANNONCES_EN_LIGNE_MOIS_PASSE)
		VALUES (S.ID_ORGANISME
			,S.NOM_ORGANISME
			,S.ORGANISME_VALIDE
			,S.CODE_POSTAL
			,S.VILLE
			,S.REGION
			,S.ID_TYPE_ORGANISME
			,S.PACK_VISIBILITE
			,S.PACK_ENTREPRISE_V2
			,S.PAGE_EMPLOYEUR_V2_PUBLIEE
			,S.NBR_JETONS_ACHETES
			,S.DATE_EXPIRATION_JETONS
			,S.NBR_JETONS_RESTANT
			,S.NBR_ANNONCES_TOTALES
			,S.NBR_ANNONCES_ACTIVES
			,S.NBR_ANNONCES_EN_LIGNE_MOIS
			,S.NBR_ANNONCES_EN_LIGNE_MOIS_PASSE)
WHEN MATCHED
	THEN 
		UPDATE SET
			T.NOM_ORGANISME=S.NOM_ORGANISME
			,T.ORGANISME_VALIDE=S.ORGANISME_VALIDE
			,T.CODE_POSTAL=S.CODE_POSTAL
			,T.VILLE=S.VILLE
			,T.REGION=S.REGION
			,T.ID_TYPE_ORGANISME=S.ID_TYPE_ORGANISME
			,T.PACK_VISIBILITE=S.PACK_VISIBILITE
			,T.PACK_ENTREPRISE_V2=S.PACK_ENTREPRISE_V2
			,T.PAGE_EMPLOYEUR_V2_PUBLIEE=S.PAGE_EMPLOYEUR_V2_PUBLIEE
			,T.NBR_JETONS_ACHETES=S.NBR_JETONS_ACHETES
			,T.DATE_EXPIRATION_JETONS=S.DATE_EXPIRATION_JETONS
			,T.NBR_JETONS_RESTANT=S.NBR_JETONS_RESTANT
			,T.NBR_ANNONCES_TOTALES=S.NBR_ANNONCES_TOTALES
			,T.NBR_ANNONCES_ACTIVES=S.NBR_ANNONCES_ACTIVES
			,T.NBR_ANNONCES_EN_LIGNE_MOIS=S.NBR_ANNONCES_EN_LIGNE_MOIS
			,T.NBR_ANNONCES_EN_LIGNE_MOIS_PASSE=S.NBR_ANNONCES_EN_LIGNE_MOIS_PASSE;
			
			
			
			
			