SELECT ACTION_WEKA_EMT.UNIQUE_ID,SENT_DT,SUM(CASE 	WHEN PROBEID='-1' THEN 1 	ELSE 0	END) AS NB_OUVERTURE,SUM(CASE 	WHEN PROBEID<>'-1' THEN 1 	ELSE 0	END) AS NB_CLIC,0 AS STATUT_LIVRAISONFROM ACTIONQUEUE WITH (NOLOCK) LEFT JOIN FLAGS WITH (NOLOCK) ON FLAGS.INQUEUEID = ACTIONQUEUE.INQUEUEID INNER JOIN ACTION_WEKA_EMT WITH (NOLOCK) ON ACTION_WEKA_EMT.ID = ACTIONQUEUE.DITEMID AND ENVIRONNEMENT=1WHERE SENT_DT >= GETDATE()-7AND EXISTS (SELECT 1 FROM CAMPAIGNS WITH (NOLOCK) WHERE CAMPAIGNS.ID =ACTIONQUEUE.CAMPAIGNID AND CATEGORY = 'WEKA_EMT')GROUP BY ACTION_WEKA_EMT.UNIQUE_ID,SENT_DT;

ACTION_WEKA_EMT_ID, 	LONG
UNIQUE_ID,				TEXT 40
SENT_DT,				DATETIME
NB_OUVERTURE,			INT	
NB_CLIC,				INT
STATUT_LIVRAISON, 		INT
ENVIRONNEMENT			INT
INQUEUEID,				LONG
DITEMID					LONG



INSERT INTO DATA_WEKA_JOBS_EMT_STATS (ACTION_WEKA_EMT_ID,UNIQUE_ID,ENVIRONNEMENT,CAMPAIGNID,SENT_DT)select ID,UNIQUE_ID,ENVIRONNEMENT,CAMPAIGNID,EXEC_DT from ACTION_WEKAJOBS_EMT with (nolock)where not exists (select 1 from DATA_WEKA_JOBS_EMT_STATS where DATA_WEKA_JOBS_EMT_STATS.ACTION_WEKA_EMT_ID = ACTION_WEKAJOBS_EMT.ID)and environnement =1 and exec_dt is not null;UPDATE STATSSET STATS.INQUEUEID=ACTIONQUEUE.INQUEUEID, STATS.SMTPERR=ACTIONQUEUE.SMTPERR FROM DATA_WEKA_JOBS_EMT_STATS STATSINNER JOIN ACTIONQUEUE WITH (NOLOCK) ON STATS.ACTION_WEKA_EMT_ID = ACTIONQUEUE.DITEMID AND STATS.CAMPAIGNID = ACTIONQUEUE.CAMPAIGNIDWHERE STATS.INQUEUEID IS NULL OR STATS.SMTPERR IS NULL;UPDATE DATA_WEKA_JOBS_EMT_STATSSET NB_CLIC = (SELECT count(*) FROM FLAGS WITH (NOLOCK) WHERE FLAGS.INQUEUEID = DATA_WEKA_JOBS_EMT_STATS.INQUEUEID AND DATA_WEKA_JOBS_EMT_STATS.CAMPAIGNID = FLAGS.CAMPAIGNID and probeid > 1)where sent_dt > getdate()-8;-- rajouter date d'envoi 7 joursUPDATE DATA_WEKA_JOBS_EMT_STATSSET NB_OUVERTURE = (SELECT count(*) FROM FLAGS WITH (NOLOCK) WHERE FLAGS.INQUEUEID = DATA_WEKA_JOBS_EMT_STATS.INQUEUEID AND DATA_WEKA_JOBS_EMT_STATS.CAMPAIGNID = FLAGS.CAMPAIGNID and PROBEID='-1')where sent_dt > getdate()-8;SELECT UNIQUE_ID,SENT_DT,NB_OUVERTURE,NB_CLIC,SMTPERR AS STATUT_LIVRAISON FROM DATA_WEKA_JOBS_EMT_STATSwhere SENT_DT > GETDATE()-8AND environnement =1;