------------------------------------------flux 128 liste des regions -------------------------------------------------------------

TRUNCATE TABLE DATA_WKJOBS_REF_REGIONS;

INSERT INTO DATA_WKJOBS_REF_REGIONS (ID_REGION, LIBELLE)
SELECT ID_REGION, LIBELLE FROM TMP_WKJOBS_REGIONS
WHERE SIM_PARSELINE_RESULT = 1;


------------------------------------------flux 127 liste des cadres emplois -------------------------------------------------------------

TRUNCATE TABLE DATA_WKJOBS_REF_CADRES_EMPLOIS;

INSERT INTO DATA_WKJOBS_REF_CADRES_EMPLOIS (ID_CADRE_EMPLOI, LIBELLE)
SELECT ID_CADRE_EMPLOI, LIBELLE FROM TMP_WKJOBS_CADRES_EMPLOIS
WHERE SIM_PARSELINE_RESULT = 1;

------------------------------------------flux 126 liste des filieres -------------------------------------------------------------

TRUNCATE TABLE DATA_WKJOBS_REF_FILIERES;

INSERT INTO DATA_WKJOBS_REF_FILIERES (ID_FILIERE, LIBELLE)
SELECT ID_FILIERE, LIBELLE FROM TMP_WKJOBS_FILIERES
WHERE SIM_PARSELINE_RESULT = 1;

------------------------------------------flux 125 liste des metier -------------------------------------------------------------

TRUNCATE TABLE DATA_WKJOBS_REF_METIERS;

INSERT INTO DATA_WKJOBS_REF_METIERS (ID_METIER, LIBELLE)
SELECT ID_METIER, LIBELLE FROM TMP_WKJOBS_METIERS
WHERE SIM_PARSELINE_RESULT = 1;

------------------------------------------flux 124 liste des utilisateurs -------------------------------------------------------------

TRUNCATE TABLE DATA_WKJOBS_CPTM_UTILISATEURS;

INSERT INTO DATA_WKJOBS_CPTM_UTILISATEURS (ID_UTILISATEUR, ID_ANNONCE, CANDIDATURE, DATE_CANDIDATURE, BOOKMARK, DATE_BOOKMARK)
SELECT ID_UTILISATEUR, ID_ANNONCE, CANDIDATURE, DATE_CANDIDATURE, BOOKMARK, DATE_BOOKMARK FROM TMP_WKJOBS_UTILISATEUR
WHERE SIM_PARSELINE_RESULT = 1;


------------------------------------------flux 114 liste des annonces -------------------------------------------------------------

TRUNCATE TABLE DATA_WKJOBS_ANNONCES;

INSERT INTO DATA_WKJOBS_ANNONCES (
INTITULE, LOGO, ID_DOMAINE_ACTIVITE, ID_ORGANISME, TYPE_CONTRAT, CODE_POSTAL, VILLE, ID_REGION, EXTRAIT, URL, DATE_PUBLICATION, DATE_FIN_PUBLICATION, NBR_VUE, NBR_CLIC, PAYANTE, CATEGORIE, ID_METIER, ID_FILIERE, ID_CADRE_EMPLOI, NBR_CANDIDATURE )
SELECT INTITULE, LOGO, ID_DOMAINE_ACTIVITE, ID_ORGANISME, TYPE_CONTRAT, CODE_POSTAL, VILLE, ID_REGION, EXTRAIT, URL, DATE_PUBLICATION, DATE_FIN_PUBLICATION, NBR_VUE, NBR_CLIC, PAYANTE, CATEGORIE, ID_METIER, ID_FILIERE, ID_CADRE_EMPLOI, NBR_CANDIDATURE
FROM TMP_WKJOBS_ANNONCE
WHERE SIM_PARSELINE_RESULT = 1;